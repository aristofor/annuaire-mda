from flask import g, make_response, render_template, url_for
from datetime import date
from os import listdir
from os.path import join
from .core.flaskapp import create_app
from .core.render import render_markdown
from .content.models import Content
from .entry.models import Entry
from .auth import mod as auth_module, jwt
from .account.views import mod as account_module
from .entry.views import mod as entry_module

#######################################################################################################################

app = create_app(__name__)
jwt.init_app(app)
app.register_blueprint(auth_module, url_prefix='/api')
app.register_blueprint(account_module, url_prefix='/api')
app.register_blueprint(entry_module, url_prefix='/api')

#######################################################################################################################


@app.before_request
def before_request():
    """
    ajoute au contexte :
        config de la carte
        taille de page
    """
    g.config = {k: app.config[k] for k in ('MAP_DEFAULT_LATLNG', 'MAP_TILELAYER', 'MAP_TILELAYER_OPTIONS')}
    g.config['PAGE_SIZE'] = 6


#######################################################################################################################


@app.route('/')
def home():
    return "OK"


@app.route('/api/home')
def api_home():
    """
    Page d'accueil
        Bannière du jour
        Entrées récentes
    """
    banners = sorted(listdir(join(app.static_folder, 'banner')))
    banner_idx = (date.today() - date(2020, 1, 1)).days % len(banners)
    banner = banners[banner_idx]
    recent = list()
    for item in Entry.query.filter(Entry.active == True).order_by(Entry.id.desc()).limit(3).all():
        if item.avatar.exists:
            avatar = url_for('asset', filename=item.avatar.filename, t=item.avatar.mtime)
        else:
            avatar = url_for('static', filename='img/default_75x75.png')
        item = dict(nom=item.nom, slug=item.slug, avatar=avatar)
        recent.insert(0, item)
    return dict(banner=banner, recent=recent)


@app.route('/api/cgu')
def cgu():
    content = Content.query.filter(Content.slug == 'cgu').first()
    html = render_markdown(content.body)
    return dict(html=html)


@app.route('/api/apropos')
def apropos():
    from . import __version__
    content = Content.query.filter(Content.slug == 'apropos').first()
    body = ''.join((content.body, "\n\nVersion ", __version__,
                    '\n\n<a href="https://gitlab.com/aristofor/annuaire-mda" target="_blank">'
                    '<i class="fab fa-gitlab"></i> Source</a>'))
    html = render_markdown(body)
    return dict(html=html)


@app.route('/api/map_config')
def map_config():
    center = g.config['MAP_DEFAULT_LATLNG']
    tile_layer = dict(url=g.config['MAP_TILELAYER'], **g.config['MAP_TILELAYER_OPTIONS'])
    return dict(center=center, tileLayer=tile_layer)


#######################################################################################################################


@app.route('/map')
def map():
    return render_template('map.html')


@app.route('/rss')
def rss():
    recent = Entry.query.order_by(Entry.id.desc()).limit(3).all()
    xml = render_template('rss.xml', recent=reversed(recent))
    response = make_response(xml)
    response.headers['Content-Type'] = 'application/rss+xml'
    return response
