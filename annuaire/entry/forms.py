from flask_wtf import FlaskForm
from flask_wtf.file import FileField
from wtforms import ValidationError
from wtforms.fields import BooleanField, HiddenField, SelectField, StringField, TextAreaField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Length, Email, Optional
from wtforms.widgets import TextArea
import annuaire.entry.constants as ASSO
from .models import Entry

#######################################################################################################################

from collections import OrderedDict
import re


class TextAreaField_Tags(TextAreaField):
    """
    Supprime les doublons de la chaîne
    """

    def process_formdata(self, valuelist):
        result = list()
        seen = set()
        if valuelist:
            for x in re.split('\s*,\s*', valuelist[0]):
                x = x.strip()
                if len(x):
                    k = x.lower()
                    if k not in seen:
                        seen.add(k)
                        result.append(x)
            valuelist[0] = ','.join(result)
        super().process_formdata(valuelist)


class EntryEditForm(FlaskForm):
    """
    Édition d'Asso
    """

    class Meta:
        csrf = False

    nom = StringField("Nom complet", validators=[DataRequired(), Length(1, 255)])
    active = BooleanField("Association active")
    description = TextAreaField("Description", render_kw={'rows': 6}, validators=[DataRequired()])
    tags = TextAreaField_Tags("Activités (mots clés)",
                              render_kw={
                                  'rows': 2,
                                  'class': "textarea"
                              },
                              validators=[Optional()])
    rubrique = SelectField("Rubrique", choices=[(k, v) for k, v in ASSO.RUBRIQUE_LABEL.items()], coerce=int)
    adresse = StringField("Adresse", validators=[DataRequired()])
    codepostal = StringField("Code postal", validators=[DataRequired(), Length(1, 255)])
    ville = StringField("Ville", validators=[DataRequired(), Length(1, 255)])
    contact = StringField("Personne à contacter", validators=[Optional(), Length(1, 255)])
    telephone = StringField("Téléphone", validators=[Optional(), Length(1, 14)])
    email = EmailField("Adresse e-mail", validators=[Optional(), Length(1, 254), Email()])
    siteweb = StringField("Site web", validators=[Optional(), Length(1, 255)])
    geoloc_lat = HiddenField()
    geoloc_lng = HiddenField()
    geoloc_ok = BooleanField()
    image = FileField("Image", validators=[Optional()])
