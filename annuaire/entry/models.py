from flask import current_app
from sqlalchemy import event, func
import unicodedata
import re
from functools import reduce
from sqlalchemy_utils import TSVectorType
from ..core.database import db
from ..core.models import MtimeMixin
import annuaire.entry.constants as ENTRY

#######################################################################################################################


class Entry(db.Model, MtimeMixin):
    """
    Entrée d'annuaire
    """

    id = db.Column(db.Integer, primary_key=True)

    # identifiant Account parent
    account_id = db.Column(db.Integer, db.ForeignKey('account.id'), nullable=True)
    account = db.relationship('Account',
                              backref=db.backref('entries', uselist=True, cascade='all', order_by='Entry.nom.asc()'))

    # Nom de l'asso
    nom = db.Column(db.String(255), index=True)

    # nom unique (pour construction url)
    slug = db.Column(db.String(255), default=None, unique=True)

    # Association active
    active = db.Column(db.Boolean, server_default='t')

    def __str__(self):
        return self.nom

    def __repr__(self):
        return f'Entrée:{self.id}'

    rubrique = db.Column(db.SmallInteger, default=0)

    # nom du contact
    contact = db.Column(db.String(255), default='')
    # e-mail du contact
    email = db.Column(db.String(255), default='')
    # téléphone du contact
    telephone = db.Column(db.String(20), default='')

    # adresse
    adresse = db.Column(db.String(255), default='')
    # code postal
    codepostal = db.Column(db.String(6), default='')
    # ville
    ville = db.Column(db.String(80), default='')

    description = db.Column(db.Text, default='')

    # activités
    tags = db.Column(db.String(900))

    siteweb = db.Column(db.String(255))

    @property
    def siteweb_url(self):
        """
        ajoute 'http' à siteweb si aucun scheme n'est spécifié
        """
        from urllib.parse import urlsplit, urlunsplit
        if not self.siteweb:
            return ''
        s = list(urlsplit(self.siteweb))
        if not s[0]:
            s[0] = 'http'
        return urlunsplit(s)

    # statut de la géolocalisation
    geoloc_ok = db.Column(db.Boolean, default=False)
    # latitude
    geoloc_lat = db.Column(db.Float)
    # longitude
    geoloc_lng = db.Column(db.Float)

    # text search vector
    tsv = db.Column(TSVectorType)

    __table_args__ = (db.Index("ix_content_tsv", tsv, postgresql_using='GIN'),)

    @classmethod
    def slugify(cls, target):
        """
        Détermine un slug à partir de Entry.nom
        """
        trans = unicodedata.normalize('NFKD', target.nom).encode('ascii', 'ignore').decode('ascii').lower()
        slug = re.sub('[^\w\s\'-]', '', trans).strip(" '-")
        slug = re.sub('[-\s\']+', '-', slug)
        qs = cls.query.filter(cls.id!=target.id). \
            filter(cls.slug.op('~')('^'+slug+'(-\d+)?$')).with_entities(cls.slug)
        if qs.count():
            at = len(slug) + 1
            nl = [int(d[0][at:] or 0) for d in qs.all()]
            m = reduce(lambda x, y: max(x, y), nl)
            slug = slug + '-' + str(m + 1)
        return slug

    _avatar = None

    @property
    def avatar(self):
        if self._avatar is None:
            self._avatar = Avatar(self)
        return self._avatar


#######################################################################################################################


@event.listens_for(Entry.nom, 'set', retval=False)
def entry_set_nom(target, value, oldvalue, initiator):
    """
    Invalide Entry.slug quand on modifie Entry.nom
    """
    if oldvalue != value:
        target.slug = None


@event.listens_for(Entry, 'before_update')
@event.listens_for(Entry, 'before_insert')
def entry_before_insert(mapper, connection, target):
    """
    Reconstruit un slug si slug = None ou ''
    """
    if not target.slug:
        target.slug = Entry.slugify(target)
    target.tsv = func.to_tsvector('french',
                                  (target.nom or '') + '\n' + (target.description or '') + '\n' + (target.tags or ''))


@event.listens_for(Entry, 'before_delete')
def entry_before_delete(mapper, connection, target):
    """
    Efface l'avatar, déindexe le document
    """
    target.avatar.unlink()


from sqlalchemy import DDL

update_entry_tags = DDL('''
CREATE OR REPLACE FUNCTION process_entry_tags()
RETURNS trigger AS
$BODY$
	BEGIN
	IF (TG_OP = 'UPDATE') THEN
		DELETE FROM tag WHERE entry_id=NEW.id;
	END IF;
	INSERT INTO tag
		SELECT NEW.id,a FROM
		(SELECT regexp_split_to_table(LOWER(NEW.tags),'\s*,\s*') AS a GROUP BY a)
		AS u WHERE u.a!='' AND u.a IS NOT NULL;
	RETURN NULL;
	END;
$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_process_entry_tags
AFTER INSERT OR UPDATE ON entry
FOR EACH ROW EXECUTE PROCEDURE process_entry_tags();
''')
event.listen(Entry.__table__, 'after_create', update_entry_tags)

#######################################################################################################################


class Tag(db.Model):
    entry_id = db.Column(db.Integer,
                         db.ForeignKey('entry.id', onupdate="CASCADE", ondelete="CASCADE"),
                         primary_key=True)
    rep = db.Column(db.String(60), primary_key=True)


#######################################################################################################################

from os import listdir, makedirs, stat
from os.path import dirname, isdir, isfile, join
from PIL import Image
from shutil import rmtree


class Avatar:

    def __init__(self, parent):
        self.entry_id = parent.id
        self.prefix = join(current_app.config['VAR_DIR'], 'files')

    def shard(self):
        """
        Formatte entry_id en chemin fragmenté
        exemples:
               1 -> 1/0/0
            1234 -> 4/3/12
        """
        s = '{:03d}'.format(self.entry_id)
        return join(s[-1:], s[-2:-1], s[:-2])

    @property
    def filename(self):
        if self.entry_id is None:
            raise RuntimeError("Avatar.filename() : entry_id manquant")
        return join(self.shard(),'{}_{}x{}.png' \
                    .format(self.entry_id,*ENTRY.AVATAR_SIZE))

    @property
    def mtime(self):
        return stat(join(self.prefix, self.filename)).st_mtime

    @property
    def thumbnail(self):
        if self.entry_id is None:
            raise RuntimeError("Avatar.thumbnail() : entry_id manquant")
        return join(self.shard(),'{}_{}x{}.png' \
                    .format(self.entry_id,*ENTRY.THUMBNAIL_SIZE))

    @property
    def thumbnail_mtime(self):
        return stat(join(self.prefix, self.thumbnail)).st_mtime

    @property
    def exists(self):
        if self.entry_id is None:
            return False
        fname = join(self.prefix, self.filename)
        return isfile(fname)

    def unlink(self):
        """
        Supprime les fichiers et leur dossier.
        Supprime le dossier parent s'il est vide.
        """
        if self.entry_id is None:
            return
        dname = join(self.prefix, dirname(self.filename))
        parent = dirname(dname)
        if isdir(dname):
            rmtree(dname)
        if isdir(parent) and not len(listdir(parent)):
            rmtree(parent)

    def build(self, source):
        """
        Création des images avatar et thumbnail.
        Centrage de l'image source sur un fond transparent
        de taille AVATAR_SIZE.
        """
        if self.entry_id is None:
            return
        im = Image.open(source)
        im.thumbnail(ENTRY.AVATAR_SIZE)
        box = ((ENTRY.AVATAR_SIZE[0] - im.size[0]) // 2, (ENTRY.AVATAR_SIZE[1] - im.size[1]) // 2)
        bkgnd = Image.new("RGBA", ENTRY.AVATAR_SIZE)
        bkgnd.paste(im, box)
        dest = join(self.prefix, self.filename)
        makedirs(dirname(dest), exist_ok=True)
        bkgnd.save(dest)
        bkgnd.thumbnail(ENTRY.THUMBNAIL_SIZE)
        bkgnd.save(join(self.prefix, self.thumbnail))
