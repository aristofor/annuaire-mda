# labels des rubriques
RUBRIQUE_LABEL = {
    0: "Autres",
    1: "Actions caritatives et humanitaires",
    2: "Action sociale",
    3: "Culture",
    4: "Défense des droits et des causes",
    5: "Développement local",
    6: "Éducation et Formation",
    7: "Économie",
    8: "Environnement",
    9: "Handicap",
    10: "Jeunesse",
    11: "Santé",
    12: "Sports et Loisirs",
    13: "Numérique",
}

# dimensions d'un avatar
AVATAR_SIZE = (200,200)

# dimensions d'une miniature d'avatar
THUMBNAIL_SIZE = (75,75)
