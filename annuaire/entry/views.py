from flask import abort, Blueprint, current_app, g, jsonify, request, url_for
from flask.views import MethodView
from flask_jwt_extended import get_jwt_identity, jwt_required, verify_jwt_in_request
from sqlalchemy import func
import re
from werkzeug.datastructures import CombinedMultiDict
from ..core.models import Activity
from ..core.render import render_simple
from .models import db, Entry, Tag
from .forms import *
import annuaire.entry.constants as ENTRY

mod = Blueprint('entry', __name__)

#######################################################################################################################


class EditEntryAPI(MethodView):

    fields = ('nom', 'rubrique', 'contact', 'email', 'tags', 'telephone', 'adresse', 'codepostal', 'ville',
              'description', 'siteweb', 'geoloc_ok', 'geoloc_lat', 'geoloc_lng', 'active')

    @jwt_required()
    def get(self, id):
        entry = Entry.query.get(id)
        if entry is None:
            abort(404)
        if get_jwt_identity() != entry.account_id:
            abort(403)
        ret = {k: getattr(entry, k) for k in self.fields}
        if entry.avatar.exists:
            ret['avatar'] = url_for('asset', filename=entry.avatar.filename, t=entry.avatar.mtime)
        else:
            ret['avatar'] = url_for('static', filename='img/default_200x200.png')
        return ret, 200

    @jwt_required()
    def put(self, id):
        # Pas de json ici
        entry = Entry.query.get(id)
        if entry is None:
            abort(404)
        if get_jwt_identity() != entry.account_id:
            abort(403)
        return self.process(entry)

    @jwt_required()
    def post(self):
        # Pas de json ici
        entry = Entry(account_id=get_jwt_identity())
        return self.process(entry, create=True)

    def process(self, entry, create=False):
        """Code commun à PUT/POST"""
        form = EntryEditForm(CombinedMultiDict((request.form, request.files)), obj=entry)
        form.validate()
        if form.errors:
            return dict(errors=form.errors)
        form.populate_obj(entry)
        message = None
        if db.session.is_modified(entry):
            """ sanitize tags """
            tags = [t[:60] for t in re.split('\s*,\s*', form.tags.data)]
            entry.tags = ','.join(tags)
            """ gestion de geolok_ok : c'est un HiddenField contenant '0' ou '1' """
            if int(form.geoloc_ok.data):
                entry.geoloc_ok = True
            else:
                entry.geoloc_ok = False
                entry.geoloc_lat = None
                entry.geoloc_lng = None
            try:
                if create:
                    db.session.add(entry)
                db.session.commit()
                if create:
                    msg = Activity(object=entry, message=u"Création d'entrée: " + entry.nom)
                    db.session.add(msg)
                    db.session.commit()
            except Exception as e:
                db.session.rollback()
                current_app.logger.error(e)
                message = "Échec de l'enregistrement."
            if form.image.data:
                ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

                def allowed_file(filename):
                    return '.' in filename and \
                        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

                filename = form.image.data.filename
                if allowed_file(filename):
                    entry.avatar.build(form.image.data)
                else:
                    message = "Échec d'enregistrement de l'image, formats autorisés : png, jpg."
        ret = dict()
        if entry.id:
            ret['slug'] = entry.slug
        if message:
            ret['message'] = message
        return ret, 201 if create else 200

    @jwt_required()
    def delete(self, id):
        entry = Entry.query.get(id)
        if entry.account_id != get_jwt_identity():
            abort(403)
        db.session.delete(entry)
        db.session.commit()
        return dict(msg="Entrée supprimée.")


edit_entry_view = EditEntryAPI.as_view('edit')
mod.add_url_rule('/edit/<int:id>', view_func=edit_entry_view, methods=('GET', 'PUT', 'DELETE'))
mod.add_url_rule('/edit', view_func=edit_entry_view, methods=('POST',))

#######################################################################################################################


def make_list_item(model, account_id=None):
    """
    Formatte model pour une utilisation comme élément de liste
    """
    data = {k: getattr(model, k) for k in ('nom', 'slug', 'tags')}
    if model.avatar.exists:
        data['avatar'] = url_for('asset', filename=model.avatar.filename, t=model.avatar.thumbnail_mtime)
    else:
        data['avatar'] = url_for('static', filename='img/default_75x75.png')
    if account_id == model.account_id:
        data['id'] = model.id
        data['editable'] = True
    return data


#######################################################################################################################


@mod.get('/recherche')
# @jwt_required(optional=True)
def search():
    page = int(request.args.get('p', 0)) or 1
    psize = g.config['PAGE_SIZE']
    q = request.args.get('q', None)
    if q is None:
        abort(404)
    tsquery = func.plainto_tsquery("french", q)
    qs = Entry.query.filter(Entry.active == True)
    qs = qs.filter(Entry.tsv.bool_op('@@')(tsquery)).order_by(func.ts_rank(Entry.tsv, tsquery).desc())
    pagination = qs.paginate(page=page, per_page=psize, count=True)
    items = list()
    try:
        verify_jwt_in_request()
        account_id = get_jwt_identity()
    except:
        account_id = None
    for model in pagination.items:
        items.append(make_list_item(model, account_id))
    return dict(page=pagination.page, perPage=pagination.per_page, total=pagination.total, items=items)


#######################################################################################################################


@mod.get('/assos')
# @jwt_required(optional=True)
def assos():
    page = request.args.get('p', 1)
    page = int(page)
    psize = g.config['PAGE_SIZE']
    qs = Entry.query.filter(Entry.active == True)
    pagination = qs.order_by(Entry.nom).paginate(page=page, per_page=psize, count=True)
    items = list()
    try:
        verify_jwt_in_request()
        account_id = get_jwt_identity()
    except:
        account_id = None
    for model in pagination.items:
        items.append(make_list_item(model, account_id))
    return dict(page=pagination.page, perPage=pagination.per_page, total=pagination.total, items=items)


@mod.get('/mes_entrees')
@jwt_required()
def my_entries():
    """
    Entrées de l'utilisateur
    """
    page = request.args.get('p', 1)
    page = int(page)
    psize = g.config['PAGE_SIZE']
    account_id = get_jwt_identity()
    qs = Entry.query.filter(Entry.account_id == account_id).order_by(Entry.nom)
    pagination = qs.paginate(page=page, per_page=psize, count=True)
    items = list()
    for model in pagination.items:
        items.append(make_list_item(model, account_id))
    return dict(page=pagination.page, perPage=pagination.per_page, total=pagination.total, items=items)


#######################################################################################################################


@mod.get('/rubriques')
def rubric_list():
    """
    Liste des rubriques
    """
    qs = Entry.query.filter(Entry.active == True)
    qs = qs.with_entities(Entry.rubrique, func.count()) \
                                    .group_by(Entry.rubrique)
    counts = dict(qs.all())
    # rubrique "Autres" en fin de liste
    res = [{
        'id': k,
        'label': ENTRY.RUBRIQUE_LABEL[k],
        'count': counts.get(k, 0)
    } for k in filter(lambda v: v != 0, ENTRY.RUBRIQUE_LABEL.keys())]
    res.append({'id': 0, 'label': ENTRY.RUBRIQUE_LABEL[0], 'count': counts[0]})
    return res


@mod.get('/rubrique/<int:rubric_id>')
# @jwt_required(optional=True)
def rubric(rubric_id):
    """
    Entrées par rubrique
    """
    page = request.args.get('p', 1)
    page = int(page)
    psize = g.config['PAGE_SIZE']
    qs = Entry.query.filter(Entry.active == True)
    qs = qs.filter(Entry.rubrique == rubric_id).order_by(Entry.nom)
    pagination = qs.paginate(page=page, per_page=psize, count=True)
    items = list()
    try:
        verify_jwt_in_request()
        account_id = get_jwt_identity()
    except:
        account_id = None
    for model in pagination.items:
        items.append(make_list_item(model, account_id))
    return dict(label=ENTRY.RUBRIQUE_LABEL[rubric_id],
                page=pagination.page,
                perPage=pagination.per_page,
                total=pagination.total,
                items=items)


@mod.get('/rub_labels')
def rub_labels():
    """
    id et labels des rubriques
    """
    ret = [{'id': k, 'label': v} for k, v in ENTRY.RUBRIQUE_LABEL.items()]
    return jsonify(ret), 200


#######################################################################################################################


@mod.get('/activites')
def tag_list():
    """
    Liste des activités
    """
    qs = Tag.query.join(Entry).filter(Entry.active == True) \
            .with_entities(Tag.rep, func.count()).group_by( Tag.rep).order_by(Tag.rep)
    ret = [{'label': r[0], 'count': r[1]} for r in qs.all()]
    return jsonify(ret), 200


@mod.get('/activite/<path:rep>')
# @jwt_required(optional=True)
def tag(rep):
    """
    Entrées par activité
    """
    page = request.args.get('p', 1)
    page = int(page)
    psize = g.config['PAGE_SIZE']
    qs = Entry.query.filter(Entry.active == True)
    qs = qs.join(Tag).filter(Tag.rep == rep).order_by(Entry.nom)
    pagination = qs.paginate(page=page, per_page=psize, count=True)
    items = list()
    try:
        verify_jwt_in_request()
        account_id = get_jwt_identity()
    except:
        account_id = None
    for model in pagination.items:
        items.append(make_list_item(model, account_id))
    return dict(page=pagination.page, perPage=pagination.per_page, total=pagination.total, items=items)


#######################################################################################################################


@mod.get('/asso/<path:slug>')
@mod.get('/asso', defaults={'slug': None})
# @jwt_required(optional=True)
def detail(slug=None):
    """
    Détail d'une entrée
    """
    if slug is None:
        entry_id = int(request.args.get('id'))
        if entry_id:
            model = Entry.query.get(entry_id)
        else:
            abort(400)
    else:
        model = Entry.query.filter(Entry.slug == slug).one_or_none()
    if model is None:
        abort(404)
    attrs = ('nom', 'contact', 'email', 'telephone', 'siteweb', 'tags', 'adresse', 'codepostal', 'ville')
    ret = {k: getattr(model, k) for k in attrs}
    ret['description'] = render_simple(model.description)
    ret['siteweb_url'] = model.siteweb_url
    if model.avatar.exists:
        ret['avatar'] = url_for('asset', filename=model.avatar.filename, t=model.avatar.mtime)
    else:
        ret['avatar'] = url_for('static', filename='img/default_200x200.png')
    if model.geoloc_ok:
        ret['geoloc'] = (model.geoloc_lat, model.geoloc_lng)
    try:
        verify_jwt_in_request()
        account_id = get_jwt_identity()
        if account_id == model.account_id:
            ret['id'] = model.id
            ret['editable'] = True
    except Exception:
        pass
    return ret


#######################################################################################################################


@mod.get('/markers')
def markers():
    """
    Tous les markers
    """
    ret = list()
    qs = Entry.query.filter(Entry.active == True)
    for item in qs.filter(Entry.geoloc_ok==True) \
                .with_entities(Entry.id, Entry.geoloc_lat, Entry.geoloc_lng).all():
        ret.append((item.id, item.geoloc_lat, item.geoloc_lng))
    return jsonify(ret)


@mod.get('/marker/<int:id>')
def marker_detail(id):
    """
    Détail d'un marker
    """
    model = Entry.query.filter(Entry.id == id and Entry.geoloc_ok == True).one_or_none()
    if model is None:
        abort(404)
    attrs = ('id', 'nom', 'adresse')
    res = {k: getattr(model, k) for k in attrs}
    res['url'] = url_for('home') + 'asso/' + model.slug
    if model.avatar.exists:
        res['thumbnail'] = url_for('asset', filename=model.avatar.thumbnail, t=model.avatar.thumbnail_mtime)
    else:
        res['thumbnail'] = url_for('static', filename='img/default_75x75.png')
    return res
