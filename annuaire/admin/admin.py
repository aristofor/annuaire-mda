from flask_admin import Admin
from annuaire.core.models import Activity
from annuaire.account.models import Account
from annuaire.entry.models import Entry
from annuaire.content.models import Content

from .views.index import AnnuaireAdminIndexView
from .views.activity import AdminActivityView
from .views.account import AdminAccountView
from .views.entry import AdminEntryView
from .views.content import AdminContentView

class AnnuaireAdmin(Admin):

    def __init__(self, app, db, *args, **kwargs):
        kwargs.setdefault('index_view', AnnuaireAdminIndexView(name="Accueil"))
        super().__init__(app, *args, **kwargs)
        self.add_view(AdminActivityView(Activity, db.session, endpoint='activity', name="Activité"))
        self.add_view(AdminAccountView(Account, db.session, endpoint='account', name="Comptes"))
        self.add_view(AdminEntryView(Entry, db.session, endpoint='entry', name="Assos"))
        self.add_view(AdminContentView(Content, db.session, endpoint='content', name="Rédactionnel"))
