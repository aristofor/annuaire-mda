from flask_login import UserMixin
from annuaire.core.database import db
from annuaire.core.models import PasswordHash

class AdminUser(db.Model, UserMixin, PasswordHash):

    id = db.Column(db.Integer, primary_key=True)
    active = db.Column(db.Boolean, default=True)
    username = db.Column(db.String(20), unique=True)
    email = db.Column(db.String(254))

    def __str__(self):
        return self.username
