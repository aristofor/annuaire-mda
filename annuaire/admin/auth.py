# coding: utf-8

from flask import redirect, flash, url_for, request, g
from flask_login import LoginManager, current_user, login_user, logout_user
from flask_admin.base import expose, BaseView, MenuLink
from flask_wtf import FlaskForm
from wtforms.fields import TextField
from wtforms.validators import DataRequired
from .models import AdminUser

BaseView.is_accessible = lambda self: current_user.is_authenticated

lm = LoginManager()
lm.login_view = 'auth.login'


@lm.user_loader
def load_user(id):
    user = AdminUser.query.get(id)
    if user is None:
        return lm.anonymous_user
    else:
        return user


class LoginForm(FlaskForm):
    username = TextField('Identifiant', validators = [DataRequired()])
    password = TextField('Mot de passe', validators = [DataRequired()])


class AuthenticatedMenuLink(MenuLink):
    def is_accessible(self):
        return current_user.is_authenticated


class AuthView(BaseView):

    def is_accessible(self):
        return True

    def is_visible(self):
        return False

    @expose('/')
    def index(self):
        return redirect(url_for('auth.login'))

    @expose('/login', methods=('GET','POST'))
    def login(self):
        if not current_user is None and current_user.is_authenticated:
            return redirect(url_for('admin.index'))
        form = LoginForm()
        status_code = 200
        if form.validate_on_submit():
            user = AdminUser.query.filter(AdminUser.username==form.username.data).first()
            if user and user.verify_password(form.password.data):
                if not user.active:
                    flash("Votre compte est désactivé.", 'danger')
                    return redirect(url_for('.login'))
                login_user(user)
                flash('Bienvenue, %s' % user.username, 'success')
                next_url = request.args.get('next')
                if not next_url:
                    next_url = url_for('admin.index')
                return redirect(next_url)
            flash('Login incorrect.', 'error')
            status_code = 401
        return BaseView.render(self, 'login.html', form=form), status_code

    @expose('/logout')
    def logout(self):
        logout_user()
        flash(u"Vous avez été déconnecté.",'info')
        return redirect(url_for('admin.index'))
