"""
Admin app
"""

from flask import Flask, redirect, url_for, request, g
from annuaire.core.flaskapp import create_app, db
from .admin import AnnuaireAdmin

#######################################################################################################################

app = create_app(__name__)
app.config.update(SESSION_COOKIE_NAME='admin')

admin = AnnuaireAdmin(app, db, name="Annuaire", template_mode='bootstrap4')

from .auth import AuthView, lm, AuthenticatedMenuLink, current_user
lm.setup_app(app)

admin.add_view(AuthView(endpoint='auth', name="Login"))
admin.add_link(AuthenticatedMenuLink("Logout", endpoint='auth.logout'))

@app.route('/')
def home():
    return redirect(url_for('admin.index'))

@app.errorhandler(403)
def redirect_login(e):
    return redirect(url_for(lm.login_view, next=request.url))
