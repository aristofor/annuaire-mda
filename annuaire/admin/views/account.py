from flask_admin.contrib.sqla import ModelView
from wtforms import PasswordField

class AdminAccountView(ModelView):

    column_list = ('email','laston',)
    column_searchable_list = ('email',)
    form_excluded_columns = ('assos',)
    form_widget_args = dict(
        password_hash = dict(disabled=True),
        laston = dict(disabled=True),
        )
    form_args = dict(
        active = dict(label="Compte activé"),
        laston = dict(label="Dernier accès"),
        email = dict(label="Adresse e-mail"),
        )

    def scaffold_form(self):
        form_class = super(AdminAccountView, self).scaffold_form()
        form_class.new_password = PasswordField('Mot de passe')
        return form_class

    def on_model_change(self, form, model, is_created):
        if len(form.new_password.data):
            model.set_password(form.new_password.data)
