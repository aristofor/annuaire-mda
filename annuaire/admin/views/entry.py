from flask_admin.contrib.sqla import ModelView
from wtforms.fields import SelectField
import annuaire.entry.constants as ENTRY

class AdminEntryView(ModelView):

    column_display_pk = True
    column_searchable_list = ('nom','account.email','id','tags')
    column_default_sort = ('mtime','desc')
    column_list = ('id','nom','account',)
    form_overrides = dict(
        rubrique = SelectField,
        )
    form_args = dict(
        rubrique = dict(choices=list(ENTRY.RUBRIQUE_LABEL.items()), coerce=int),
        telephone = dict(label="Téléphone"),
        tags = dict(label="Activités"),
        )
    form_widget_args = dict(
        description = dict(rows=4),
        )
