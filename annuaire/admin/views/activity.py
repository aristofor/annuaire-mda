from flask_admin.contrib.sqla import ModelView
from markupsafe import Markup, escape
from flask import url_for
from .. import formatters


def object_format(view, context, model, name):
    result = escape(repr(model.object))
    type_map = {
        'Entry': 'entry.edit_view',
        'Account': 'account.edit_view',
    }
    if model.object_type in type_map:
        link = url_for(type_map[model.object_type],id=model.object_id)
        result = '<a href="{}">{}</a>'.format(link,result)
    return Markup(result)


class AdminActivityView(ModelView):

    column_list = ('mtime','object','message',)
    column_labels = dict(mtime='Date', object='Cible')
    column_default_sort = ('mtime','desc')
    column_formatters = dict(
        mtime = formatters.datetimeformat,
        object = object_format,
        )
    can_create = False
    can_edit = False
