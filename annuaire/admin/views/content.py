from flask_admin.contrib.sqla import ModelView

class AdminContentView(ModelView):

    column_list = ('slug','comment',)
    column_labels = dict(comment='Description')
    column_default_sort = ('slug')
    form_widget_args = dict(
        body = dict(rows=12),
        )
    form_args = dict(
        body = dict(label="Corps de texte (markdown)"),
        comment = dict(label='Description'),
        )
