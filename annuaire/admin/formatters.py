def datetimeformat(view, context, model, name):
    """
    Formattage de timestamp :
        column_formatters = dict(
            mtime = formatters.datetimeformat,
            ...
        )
    """
    v = getattr(model,name)
    if v is None:
        return ''
    else:
        return v.strftime("%F %T")
