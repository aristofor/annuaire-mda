import click
from flask import current_app
from subprocess import run, PIPE
import tarfile
from tempfile import TemporaryDirectory
from os import mkdir
from os.path import dirname, join
from shutil import rmtree
from sqlalchemy.engine.url import make_url
import re
from ..entry.models import Avatar, Entry
from ..app import app


@click.command()
@click.argument('filename')
def cli(filename):
    """
    Restauration de l'annuaire
    """
    with app.app_context():
        click.echo(f"Restore : {filename}")
        avatar_dir = join(current_app.config['VAR_DIR'], 'files')
        with tarfile.open(filename, 'r:bz2') as tar:
            for tarfname in tar.getnames():
                if tarfname == 'annuaire_mda.pg':
                    tmpdname = TemporaryDirectory(prefix='annuaire_mda').name
                    tar.extract(tarfname, tmpdname)
                    sa_url = make_url(current_app.config['SQLALCHEMY_DATABASE_URI'])
                    completed = run(('pg_restore', '-w', '-c', '-h', sa_url.host, '-p', str(sa_url.port or 5432), '-U',
                                     sa_url.username, '-d', sa_url.database, join(tmpdname, 'annuaire_mda.pg')),
                                    env={'PGPASSWORD': sa_url.password},
                                    stderr=PIPE)
                    rmtree(tmpdname)
                else:
                    match = re.match('^(?P<id>\d+)_.*\.png$', tarfname)
                    if match:
                        entry_id = int(match['id'])
                        avatar = Avatar(parent=Entry(id=entry_id))
                        tar.extract(tarfname, join(avatar_dir, dirname(avatar.filename)))
                        avatar.build(join(avatar_dir, avatar.filename))
                    else:
                        click.echo(f"Can't process file : {tarfname}")
