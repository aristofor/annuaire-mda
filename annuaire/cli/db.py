import click
from ..app import app, db
from ..admin.models import AdminUser


@click.group()
def cli():
    """
    Gestion de la base de données.
    """
    pass


@cli.command('')
def create():
    """
    Crée les tables.
    """
    with app.app_context():
        db.create_all()


@cli.command('')
def drop():
    """
    Détruit les tables.
    """
    with app.app_context():
        try:
            click.confirm('Attention, les tables vont être effacées', abort=True)
        except click.exceptions.Abort:
            click.echo('Abandon')
            return
        db.drop_all()


#######################################################################################################################

if __name__ == '__main__':
    cli()
