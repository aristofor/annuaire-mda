from os.path import join
import click
from sqlalchemy import func
from ..core.database import db
from ..entry.models import Entry
from ..app import app


@click.group()
def cli():
    """
    Index des documents
    """
    pass


@cli.command('build')
def build():
    """
    Construit l'index.
    """
    with app.app_context():
        for target in Entry.query.all():
            target.tsv = func.to_tsvector('french', (target.nom or '') + '\n' + (target.description or '') + '\n' +
                                          (target.tags or ''))
        db.session.commit()


#######################################################################################################################

if __name__ == '__main__':
    cli()
