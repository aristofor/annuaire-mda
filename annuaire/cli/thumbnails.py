import click
from PIL import Image
from os.path import join
from ..entry.models import Entry
from ..entry import constants as ENTRY
from ..app import app


@click.group()
def cli():
    """
    Creation des avatars miniatures
    """
    pass


@cli.command()
def rebuild():
    with app.app_context():
        for entry in Entry.query.all():
            if not entry.avatar.exists:
                continue
            click.echo(entry.nom)
            click.echo("    " + entry.avatar.filename + " -> " + entry.avatar.thumbnail)
            im = Image.open(join(entry.avatar.prefix, entry.avatar.filename))
            im.thumbnail(ENTRY.THUMBNAIL_SIZE)
            im.save(join(entry.avatar.prefix, entry.avatar.thumbnail))
