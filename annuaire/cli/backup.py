import click
from flask import current_app
from subprocess import run
from tempfile import NamedTemporaryFile
from sqlalchemy.engine.url import make_url
from os import mkdir, unlink
from os.path import basename, join
import tarfile
from ..entry.models import Entry
from ..app import app


@click.command()
@click.argument('filename')
def cli(filename):
    """
    Sauvegarde de l'annuaire
    """
    with app.app_context():
        if not filename.endswith('.tar.bz2'):
            filename = filename + '.tar.bz2'
        click.echo(f"Backup : {filename}")
        archive = tarfile.open(filename, 'w:bz2')
        tmpfname = NamedTemporaryFile(prefix='annuaire_mda').name
        # db backup
        sa_url = make_url(current_app.config['SQLALCHEMY_DATABASE_URI'])
        completed = run(('pg_dump', '-w', '-c', '-Z9', '-Fc', '-h', sa_url.host, '-p', str(
            sa_url.port or 5432), '-U', sa_url.username, '-d', sa_url.database, '-f', tmpfname),
                        env={'PGPASSWORD': sa_url.password})
        archive.add(tmpfname, 'annuaire_mda.pg')
        unlink(tmpfname)
        # avatar backup
        avatardir = join(current_app.config['VAR_DIR'], 'files')
        for model in Entry.query.order_by(Entry.id).all():
            if model.avatar.exists:
                archive.add(join(avatardir, model.avatar.filename), basename(model.avatar.filename))
        archive.close()
