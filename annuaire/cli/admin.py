import sys
import click
from ..app import app, db
from ..admin.models import AdminUser


@click.group()
def cli():
    """
    Creation/Édition de compte administrateur
    """
    pass


@cli.command()
@click.argument('login')
def create(login):
    """
    Crée un compte administrateur.
    """
    with app.app_context():
        user = AdminUser.query.filter_by(username=login).first()
        if not user is None:
            click.echo('Le compte "%s" existe déjà.' % login)
            sys.exit(1)
        user = AdminUser(username=login)
        email = click.prompt('email')
        user.email = email
        password = click.prompt('Password', hide_input=True)
        user.set_password(password)
        db.session.merge(user)
        db.session.commit()
        click.echo('Compte admin "%s" créé.' % login)


@cli.command()
@click.argument('login')
def edit(login):
    """
    Édite un compte admin.
    """
    with app.app_context():
        user = AdminUser.query.filter_by(username=login).first()
        if user is None:
            click.echo('Compte admin "%s" inexistant.' % login)
            sys.exit(1)
        username = click.prompt('login', default=user.username)
        user.username = username
        email = click.prompt('email', default=user.email)
        user.email = email
        password = click.prompt('password', default='', hide_input=True)
        if password != '':
            user.set_password(password)
        if db.session.is_modified(user):
            db.session.merge(user)
            db.session.commit()
            click.echo("Modifications enregistrées.")


@cli.command()
@click.argument('login')
def delete(login):
    """
    Efface le compte admin.
    """
    with app.app_context():
        try:
            click.confirm('Effacer le compte "{}"?'.format(login), abort=True)
            user = AdminUser.query.filter_by(username=login).first()
            db.session.delete(user)
            db.session.commit()
            click.echo('Compte admin "{}" effacé.'.format(login))
        except click.exceptions.Abort:
            click.echo('Abandon')
            return


@cli.command()
def list():
    """
    Liste les comptes admin.
    """
    with app.app_context():
        users = AdminUser.query.order_by(AdminUser.username)
        for item in users.all():
            click.echo("{:<20} {}".format(item.username, item.email))


#######################################################################################################################

if __name__ == '__main__':
    cli()
