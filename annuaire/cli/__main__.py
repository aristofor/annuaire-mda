#!/usr/bin/env python3
"""
basé sur:
https://github.com/pallets/click/blob/master/examples/complex/complex/cli.py
"""

from os import listdir
from os.path import abspath, dirname, join
import click

cmd_folder = abspath(dirname(__file__))


class ComplexCLI(click.MultiCommand):

    def list_commands(self, ctx):
        rv = []
        for fname in listdir(cmd_folder):
            if not fname.startswith('_'):
                rv.append(fname[:-3])
        rv.sort()
        return rv

    def get_command(self, ctx, name):
        try:
            mod = __import__('annuaire.cli.' + name, None, None, ['cli'])
        except ImportError as e:
            print(e)
            return
        return mod.cli


@click.command(cls=ComplexCLI)
def cli():
    """Management de l'annuaire."""
    pass


if __name__ == '__main__':
    cli()
