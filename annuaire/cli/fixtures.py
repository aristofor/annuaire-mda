import click
import yaml
import glob
from datetime import datetime
import os.path
import subprocess
from ..app import app, db
from ..account.models import Account
from ..entry.models import Entry
from ..content.models import Content


@click.group()
def cli():
    """
    Opérations sur les données tests
    """
    pass


#######################################################################################################################

yaml.add_representer(datetime,
                     lambda dumper, value: dumper.represent_scalar('tag:yaml.org,2002:float', str(value.timestamp())))

yaml.add_representer(list,
                     lambda dumper, value: dumper.represent_sequence(u'tag:yaml.org,2002:seq', value, flow_style=True))


def dump_model(obj):
    data = dict()
    for k, v in obj.__dict__.items():
        if k.startswith('_'):
            continue
        data[k] = v
    return yaml.dump(data, explicit_start=True, allow_unicode=True)


@cli.command()
def dump():
    """
    Dump la BDD
    Crée les fichiers fixtures/*.yml
    """
    for obj in Account.query.all():
        with open('fixtures/account_%d.yml' % obj.id, 'w') as fd:
            fd.write(dump_model(obj))
    for obj in Entry.query.all():
        with open('fixtures/entry_%d.yml' % obj.id, 'w') as fd:
            fd.write(dump_model(obj))
    for obj in Content.query.all():
        with open('fixtures/content_%d.yml' % obj.id, 'w') as fd:
            fd.write(dump_model(obj))


#######################################################################################################################


def import_account(text):
    obj = Account()
    data = yaml.load(text, Loader=yaml.Loader)
    for k, v in data.items():
        if k == 'laston' and not v is None:
            v = datetime.fromtimestamp(v)
        setattr(obj, k, v)
    try:
        db.session.merge(obj)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        print(e)


def import_entry(text):
    obj = Entry()
    data = yaml.load(text, Loader=yaml.Loader)
    for k, v in data.items():
        if k == 'mtime' and not v is None:
            v = datetime.fromtimestamp(v)
        setattr(obj, k, v)
    try:
        db.session.merge(obj)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        print(e)


def import_content(text):
    obj = Content()
    data = yaml.load(text, Loader=yaml.Loader)
    for k, v in data.items():
        setattr(obj, k, v)
    try:
        db.session.merge(obj)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        print(e)


@cli.command('import')
def import_():
    """
    Importe le jeu test.
    """
    with app.app_context():
        try:
            click.confirm('Attention, la base va être écrasée', abort=True)
        except click.exceptions.Abort:
            click.echo('Abandon')
            return
        # désactive l'horodatage
        Entry.mtime_auto = False
        click.echo("Account")
        for fname in glob.glob('fixtures/account_*.yml'):
            with open(fname, 'r') as fd:
                text = fd.read()
                import_account(text)
        click.echo("Entry")
        for fname in sorted(glob.glob('fixtures/entry_*.yml')):
            with open(fname, 'r') as fd:
                text = fd.read()
                import_entry(text)
        click.echo("Content")
        for fname in glob.glob('fixtures/content_*.yml'):
            with open(fname, 'r') as fd:
                text = fd.read()
                import_content(text)
        with db.engine.connect() as conn:
            res = conn.execute('SELECT MAX(id) FROM account').first()
            conn.execute('ALTER SEQUENCE account_id_seq RESTART WITH %d' % (res[0] + 1))
            res = conn.execute('SELECT MAX(id) FROM entry').first()
            conn.execute('ALTER SEQUENCE entry_id_seq RESTART WITH %d' % (res[0] + 1))
            res = conn.execute('SELECT MAX(id) FROM content').first()
            conn.execute('ALTER SEQUENCE content_id_seq RESTART WITH %d' % (res[0] + 1))
        dest_dir = os.path.join(app.config['VAR_DIR'], 'files')
        subprocess.run(['rsync', '-avt', 'fixtures/files/', dest_dir])


#######################################################################################################################

if __name__ == '__main__':
    cli()
