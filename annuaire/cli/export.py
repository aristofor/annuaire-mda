import click
from pathlib import Path
import csv
from sqlalchemy import select
from ..app import app
from ..core.database import db
from ..account.models import Account


@click.group()
def cli():
    """
    Export de données
    """


@cli.command()
def account():
    """
    Comptes utilisateurs
    """
    with app.app_context():
        fname = Path(app.config['VAR_DIR']) / 'export/export_account.csv'
        fname.parent.mkdir(exist_ok=True)
        with fname.open('w') as fd:
            writer = csv.writer(fd, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(('id', 'email'))
            stmt = select(Account.id, Account.email).order_by(Account.id)
            for row in db.session.execute(stmt).all():
                writer.writerow(row)
