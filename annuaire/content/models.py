from ..core.database import db

class Content(db.Model):
    """
    Contenu rédactionnel
    """
    id = db.Column(db.Integer, primary_key=True)
    slug = db.Column(db.String(20), unique=True)
    comment = db.Column(db.String(60))
    body = db.Column(db.Text)
