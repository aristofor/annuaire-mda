from flask import current_app
import re
from markupsafe import Markup, escape
import hashlib
import markdown

def render_simple(text):
    paras = list()
    for p in re.split('\r?\n(\r?\n)+',text):
        paras.append("<p>%s</p>" % escape(p))
    return Markup(''.join(paras))

def render_markdown(text):
    cache = current_app.cache
    enc = text.encode('utf-8')
    cache_key = 'render-markdown-'+hashlib.md5(enc).hexdigest()
    res = cache.get(cache_key)
    if res is None:
        md = markdown.Markdown()
        res = Markup( md.convert(text) )
        cache.add(cache_key,res,timeout=3600*24*10)
    return res
