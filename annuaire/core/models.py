from sqlalchemy import event
from werkzeug.security import generate_password_hash, check_password_hash
import datetime
from sqlalchemy_utils.generic import generic_relationship
from .database import db

#######################################################################################################################

class MtimeMixin(object):
    """
    Classe de base pour l'horodatage des modifs

    Ajoute le champ 'mtime'

    exemple ::

        class MyModel(ModelBase, MtimeMixin):
            pass
    """

    #: Date et heure de modification
    mtime = db.Column(db.DateTime(), default=datetime.datetime.now, index=True)

    #: Active la mise à jour automatique de mtime, vrai par défaut.
    mtime_auto = True

    def touch(self):
        """Mise à jour de target.mtime"""
        self.mtime = datetime.datetime.now()

    @staticmethod
    def touch_obj(mapper, connection, target):
        """callback d'appel à touch()"""
        if target.mtime_auto:
            target.touch()

    @classmethod
    def __declare_last__(cls):
        """Enregistre le handlers sur before_insert et before_update

        .. note:: L'appel n'est propagé que dans le premier niveau d'héritage.

        """
        event.listen(cls, 'before_insert', cls.touch_obj)
        event.listen(cls, 'before_update', cls.touch_obj)

#######################################################################################################################

class PasswordHash(object):

    password_hash = db.Column(db.Text, default=None)

    """
    pas de property, parceque flask-admin écrira toujours l'attribut password

    @property
    def password(self):
        raise AttributeError('`password` is not a readable attribute')

    @password.setter
    def password(self, pw):
        self.password_hash = generate_password_hash(pw)
    """

    def set_password(self, pw):
        self.password_hash = generate_password_hash(pw)

    def verify_password(self, pw):
        if self.password_hash is None:
            return False
        return check_password_hash(self.password_hash, pw)

#######################################################################################################################

class Activity(db.Model,MtimeMixin):
    """
    Événement de l'appli
    """
    id = db.Column(db.Integer, primary_key=True)
    object_type = db.Column(db.String(20), nullable=False)
    object_id = db.Column(db.Integer)
    object = generic_relationship(object_type, object_id)
    message = db.Column(db.Text, default='')
