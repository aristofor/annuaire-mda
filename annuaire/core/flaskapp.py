from flask import abort, current_app, Flask, send_file
from werkzeug.security import safe_join
from flask_babel import Babel
from flask_mail import Mail
from flask_caching import Cache
from os.path import basename, isfile
from flask_migrate import Migrate
from .database import db
# importe AdminUser pour l'utiliser dans migrate
from ..admin.models import AdminUser


def asset(filename):
    """
    Renvoie un fichier asset.
    Utilisé par le serveur debug.
    """
    path = safe_join(current_app.config['VAR_DIR'], 'files', filename)
    if not isfile(path):
        abort(404)
    response = send_file(path, download_name=basename(filename))
    return response


class AnnuaireApp(Flask):
    #: mail extension
    mail = None
    #: cache extension
    cache = None


def create_app(*args, **kwargs):
    """
    Crée l'app flask et charge les extensions
    """
    app = AnnuaireApp(*args, **kwargs)
    app.config.from_object('annuaire.default_settings')
    app.config.from_envvar('ANNUAIRE_SETTINGS')
    db.init_app(app)
    db.app = app
    Migrate(app, db)
    Babel(app)
    app.mail = Mail(app)
    app.cache = Cache(app)
    app.add_url_rule('/asset/<path:filename>', 'asset', asset)
    return app
