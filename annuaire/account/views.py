from flask import abort, Blueprint, current_app, request, url_for
from flask_mail import Message
from string import ascii_letters, digits
from random import choice
from pickle import dumps, loads
from flask_jwt_extended import create_access_token, create_refresh_token, get_jwt_identity, jwt_required
from sqlalchemy import func
from urllib.parse import urlparse
from ..core.database import db
from ..core.models import Activity
from .models import Account
import re

mod = Blueprint('account', __name__)

from jinja2 import Environment, PackageLoader, select_autoescape

jinja_mail_env = Environment(loader=PackageLoader(__package__, 'mail'),
                             autoescape=select_autoescape(disabled_extensions=('txt',),
                                                          default_for_string=True,
                                                          default=True))

#######################################################################################################################

# préfixe pour la clé de cache d'inscription
CACHE_KEY_REGISTER = 'register-'
# préfixe pour la clé de cache de récupération
CACHE_KEY_RECOVER = 'recover-'
# durée de vie d'un code de confirmation
CACHE_DURATION = 3600


def generate_token():
    return ''.join(choice(ascii_letters + digits) for x in range(12))


#######################################################################################################################


@mod.route('/password', methods=('POST',))
@jwt_required()
def api_password():
    account_id = get_jwt_identity()
    account = Account.query.get(account_id)
    if not account:
        abort(404)
    req = request.get_json(force=True)
    passwd = req.get('password', None)
    if not passwd:
        abort(400)
    account.set_password(passwd)
    db.session.commit()
    ret = dict(message="Mot de passe enregistré.")
    return ret, 200


@mod.route('/register', methods=('POST',))
def register():
    req = request.get_json(force=True)
    email = req.get('email', None)
    password = req.get('password', None)
    if not (email and password):
        return dict(message="Erreur"), 200
    email = email.lower()
    if Account.query.filter(Account.email == email).first():
        return dict(message="Adresse e-mail déjà enregistrée."), 200
    if not re.match(r'^[a-z0-9]+[\._]?[a-z0-9\.\-]+[@][\w.-]+[.]\w+$', email):
        return dict(message="Adresse e-mail invalide."), 200
    token = generate_token()
    cache_key = CACHE_KEY_REGISTER + token
    account = Account(email=email)
    account.set_password(password)
    cache = current_app.cache
    cache.set(cache_key, dumps(account), timeout=CACHE_DURATION)
    if current_app.debug:
        parsed = urlparse(request.referrer)
        root_url = f'{parsed.scheme}://{parsed.netloc}/'
    else:
        root_url = url_for('home', _external=True)
    link = root_url + 'confirm/' + token
    msg = Message("Annuaire: Création de compte", recipients=[email])
    title = "Création de compte sur l'annuaire des assos"
    msg.body = title + "\n\nLien de confirmation : {}\n".format(link)
    msg.body += "Code de confirmation : {}".format(token)
    tpl_args = dict(title=title, link=link)
    msg.body = jinja_mail_env.get_template('register.txt').render(**tpl_args)
    msg.html = jinja_mail_env.get_template('register.html').render(**tpl_args)
    current_app.mail.send(msg)
    ret = dict(done=True)
    return ret, 200


@mod.route('/confirm/<string:token>')
def confirm(token):
    cache = current_app.cache
    cache_key = CACHE_KEY_REGISTER + token
    data = cache.get(cache_key)
    if data:
        account = loads(data)
        try:
            db.session.add(account)
            account.laston = func.now()
            db.session.commit()
            msg = Activity(object=account, message=f"Création de compte: {account}")
            db.session.add(msg)
            db.session.commit()
            return dict(done=True,
                        access_token=create_access_token(identity=account.id, fresh=True),
                        refresh_token=create_refresh_token(identity=account.id))
        except Exception as e:
            db.session.rollback()
            current_app.logger.error(e)
            ret = dict(message="La création du compte a échoué.")
        cache.delete(cache_key)
    else:
        ret = dict(message="Clé invalide.")
    return ret, 200


@mod.route('/recover', methods=('POST',))
def recover():
    req = request.get_json(force=True)
    email = req.get('email', None)
    if not email:
        return dict(message="E-mail incorrect."), 200
    email = email.lower()
    user = Account.query.filter(Account.email == email).one_or_none()
    if not user:
        return dict(message="E-mail inconnue."), 200
    token = generate_token()
    cache_key = CACHE_KEY_RECOVER + token
    current_app.cache.set(cache_key, dumps(user), timeout=CACHE_DURATION)
    link = url_for('home', _external=True) + 'retrieve/' + token
    msg = Message("Annuaire: Récupération de compte", recipients=[email])
    title = "Récupération de compte sur l'annuaire des assos"
    msg.body = title + "\n\nLien de confirmation : {}\n".format(link)
    msg.body += "Code de confirmation : {}".format(token)
    tpl_args = dict(title=title, link=link)
    msg.body = jinja_mail_env.get_template('recover.txt').render(**tpl_args)
    msg.html = jinja_mail_env.get_template('recover.html').render(**tpl_args)
    current_app.mail.send(msg)
    return dict(done=True), 200


@mod.route('/retrieve/<string:token>')
def retrieve(token):
    cache_key = CACHE_KEY_RECOVER + token
    cache = current_app.cache
    data = cache.get(cache_key)
    if data is None:
        return dict(message="Code incorrect.")
    account = loads(data)
    cache.delete(cache_key)
    return dict(access_token=create_access_token(identity=account.id, fresh=True),
                refresh_token=create_refresh_token(identity=account.id))
