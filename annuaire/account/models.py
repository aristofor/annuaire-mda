from flask_login import UserMixin
from ..core.database import db
from ..core.models import PasswordHash

class Account(db.Model,UserMixin,PasswordHash):

    id = db.Column(db.Integer, primary_key=True)

    #: compte activé
    active = db.Column(db.Boolean, default=True)

    @property
    def is_active(self):
        return self.active

    #: dernier login
    laston = db.Column(db.DateTime(), default=None)

    #: email de contact
    email = db.Column(db.String(254), unique=True, nullable=False)

    def __str__(self):
        return self.email

    def __repr__(self):
        return f'Compte:{self.id}'

    @property
    def username(self):
        return self.email
