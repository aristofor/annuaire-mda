all: htdocs

clean:
	find annuaire -name __pycache__ | xargs rm -rf

.PHONY: htdocs

htdocs:
	yarn build
	rsync -avt --delete build/ htdocs/
	rsync -avt annuaire/static/ htdocs/static/

install_dirs:
	mkdir -p local var/files

