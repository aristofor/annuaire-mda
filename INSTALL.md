Annuaire des assos - Installation
=================================

Environnement
-------------

~~~
$ make install_dirs
$ python3 -m venv --prompt AnnuaireMDA venv
$ . venv/bin/activate
$ pip install -r requirements.txt
~~~


### fichier `local/annuaire_settings.py`

~~~
SECRET_KEY = < python -c 'import os; print(os.urandom(24))' >
VAR_DIR = < chemin vers /var >
BABEL_DEFAULT_LOCALE = 'fr'
SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://dbuser:dbsecret@localhost/dbname'
# taille max de la requête
MAX_CONTENT_LENGTH = 1 * 1024 * 1024
~~~

cache redis
~~~
CACHE_TYPE = 'RedisCache'
CACHE_KEY_PREFIX = ''
~~~
d'autres moteurs que redis sont dispos, voir la
[doc de flask-caching](https://flask-caching.readthedocs.io/).

### Configuration de la carte

centre par défaut
~~~
MAP_DEFAULT_LATLNG = [ 48.111214, -1.672153 ]
~~~

fond mapbox
~~~
MAP_TILELAYER = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}'
MAP_TILELAYER_OPTIONS = dict(
    id = 'mapbox/streets-v11',
    accessToken = < mapbox access token>,
    attribution = 'Map data © <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom = 19,
)
~~~

fond open street map
~~~
MAP_TILELAYER = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png'
MAP_TILELAYER_OPTIONS = dict(
    attribution = '© OpenStreetMap contributors',
    maxZoom = 19,
)
~~~

Création de la base de données
------------------------------

~~~
$ sudo su
# su postgres -c psql
postgres=# CREATE DATABASE dbname OWNER dbuser ENCODING='utf-8';
~~~

Initialisation de la bdd
------------------------

~~~
$ ./cli db create
~~~

Création de compte administrateur
---------------------------------

~~~
$ ./cli admin create toto
email: toto@domain
Password:
~~~
