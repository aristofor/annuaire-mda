const { createProxyMiddleware } = require("http-proxy-middleware")
module.exports = function (app) {
    app.use(
        ["/api", "/static/img", "/static/banner", "/asset", "/rss", "/map"],
        createProxyMiddleware({
            target: "http://localhost:5000",
            changeOrigin: true
        })
    )
}
