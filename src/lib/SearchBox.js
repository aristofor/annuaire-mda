import React, { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faSearch } from "@fortawesome/free-solid-svg-icons"

export default function SearchBox(props) {
    const [q, setQ] = useState()
    const navigate = useNavigate()

    useEffect(() => {
        if (props && props.q) setQ(props.q)
    }, [props])

    const handleSearch = e => {
        e.preventDefault()
        if (!q) return
        navigate("/recherche?q=" + encodeURIComponent(q))
    }

    return (
        <form onSubmit={handleSearch}>
            <div className="field has-addons">
                <div className="control">
                    <input
                        type="text"
                        className="input"
                        name="q"
                        value={q || ""}
                        onChange={e => setQ(e.target.value)}
                        autoFocus
                        spellCheck="false"
                    />
                </div>
                <div className="control">
                    <button className="button is-link">
                        <FontAwesomeIcon icon={faSearch} />
                    </button>
                </div>
            </div>
        </form>
    )
}
