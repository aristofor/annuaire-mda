import React, { useEffect, useState } from "react"
import { render } from "react-dom"
import { TileLayer, Marker, Popup, useMap } from "react-leaflet"
import MarkerClusterGroup from "react-leaflet-markercluster"
import useMapConfig from "./MapConfig"
//import "leaflet/dist/leaflet.css"

export default function MapComponent({ dataUrl, children }) {
    const map = useMap()
    const mapConfig = useMapConfig()
    const [markers, setMarkers] = useState([])

    useEffect(() => {
        if (!mapConfig) return
        map.setView(mapConfig.center)
        fetch(dataUrl)
            .then(r => r.json())
            .then(data => {
                if (data) setMarkers(data)
            })
    }, [map, mapConfig, dataUrl])

    const onClick = evt => {
        const id = evt.target.options.id
        const popup = evt.target.getPopup()
        const el = document.createElement("div")
        fetch(`/api/marker/${id}`)
            .then(r => r.json())
            .then(data => {
                render(
                    <article className="media">
                        <figure className="media-left" style={{ margin: "0 24px 0 0" }}>
                            <p className="image is-75x75">
                                <img src={data.thumbnail} alt="" />
                            </p>
                        </figure>
                        <div className="media-content" style={{ minWidth: "200px", marginRight: "24px" }}>
                            <p className="content">
                                <a href={data.url}>{data.nom}</a>
                            </p>
                            <p className="content">{data.adresse}</p>
                        </div>
                    </article>,
                    el
                )
                popup.setContent(el)
            })
    }

    return !mapConfig ? null : (
        <>
            <TileLayer {...mapConfig.tileLayer} />
            <MarkerClusterGroup showCoverageOnHover={false}>
                {markers.map((item, idx) => (
                    <Marker key={idx} id={item[0]} position={[item[1], item[2]]} eventHandlers={{ click: onClick }}>
                        <Popup closeButton={false} />
                    </Marker>
                ))}
                {children}
            </MarkerClusterGroup>
        </>
    )
}
