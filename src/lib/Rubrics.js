import { useEffect, useState } from "react"

const cache = {}

export default function useRubrics() {
    const [data, setData] = useState(null)

    useEffect(() => {
        const fetchData = async () => {
            if (cache.current) {
                setData(cache.current)
            } else {
                const response = await fetch("/api/rub_labels")
                const data = await response.json()
                cache.current = data
                setData(data)
            }
        }
        fetchData()
    }, [])

    return data
}
