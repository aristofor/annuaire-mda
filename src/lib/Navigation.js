import { Link } from "react-router-dom"
import { AuthContext } from "../auth"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faUser } from "@fortawesome/free-solid-svg-icons"

export default function Navigation() {
    return (
        <div className="container">
            <nav className="navbar" role="navigation" aria-label="main navigation">
                <div className="navbar-brand">
                    <Link className="navbar-item" to="/">
                        <img src="/static/img/logo_annuaire.png" alt="Annuaire" width="40" height="40" />
                        &nbsp;&nbsp;
                        <span className="title">ANNUAIRE</span>
                    </Link>
                    {process.env.NODE_ENV === "development" && <div className="navbar-item">DEBUG</div>}
                    <label
                        role="button"
                        className="navbar-burger"
                        aria-label="menu"
                        aria-expanded="false"
                        htmlFor="nav-toggle-state">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </label>
                </div>
                <input type="checkbox" id="nav-toggle-state" />
                <div className="navbar-menu">
                    <div className="navbar-end">
                        <div className="navbar-item has-dropdown is-hoverable">
                            <AuthContext.Consumer>
                                {({ userInfo }) => {
                                    return userInfo ? (
                                        <>
                                            <span className="navbar-link is-arrowless">
                                                <FontAwesomeIcon icon={faUser} />
                                                &nbsp;&nbsp;
                                                <span>{userInfo.username}</span>
                                            </span>
                                            <div className="navbar-dropdown is-right">
                                                <Link className="navbar-item" to="/mes_entrees">
                                                    Mes entrées
                                                </Link>
                                                <Link className="navbar-item" to="/ajouter">
                                                    Ajouter...
                                                </Link>
                                                <Link className="navbar-item" to="/account">
                                                    Mon compte
                                                </Link>
                                                <Link to="/logout" className="navbar-item">
                                                    Déconnexion
                                                </Link>
                                            </div>
                                        </>
                                    ) : (
                                        <Link to="/login" className="navbar-item">
                                            <FontAwesomeIcon icon={faUser} />
                                            &nbsp;&nbsp; Connexion
                                        </Link>
                                    )
                                }}
                            </AuthContext.Consumer>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    )
}
