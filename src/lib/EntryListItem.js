import React from "react"
import { Link } from "react-router-dom"
import { pure } from "recompose"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEdit, faTrash } from "@fortawesome/free-solid-svg-icons"

export default pure(props => {
    const tags = props.tags ? props.tags.split(new RegExp("[ ]*,[ ]*")) : []
    return (
        <article className="media">
            <figure className="media-left">
                <p className="image is-75x75">
                    <img src={props.avatar} alt="" />
                </p>
            </figure>
            <div className="media-content">
                <div className="content">
                    <p>
                        <Link to={`/asso/${props.slug}`} className="subtitle">
                            {props.nom}
                        </Link>
                    </p>
                    {tags && (
                        <p className="tags">
                            {tags.map((rep, idx) => (
                                <span key={idx} className="tag is-link is-light">
                                    <Link to={`/activite/${encodeURIComponent(rep.toLowerCase())}`}>{rep}</Link>
                                </span>
                            ))}
                        </p>
                    )}
                </div>
            </div>
            {props.editable && (
                <div className="media-right">
                    <Link to={`/edit/${props.id}`} className="button is-small is-link">
                        <FontAwesomeIcon icon={faEdit} />
                    </Link>
                    &nbsp;
                    <button
                        onClick={_ => props.onDelete({ id: props.id, nom: props.nom })}
                        className="button is-small is-danger">
                        <FontAwesomeIcon icon={faTrash} />
                    </button>
                </div>
            )}
        </article>
    )
})
