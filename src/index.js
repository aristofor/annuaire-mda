import React from "react"
import ReactDOM from "react-dom"
//import "leaflet/dist/leaflet.css"
import "leaflet.markercluster/dist/MarkerCluster.css"
import "leaflet.markercluster/dist/MarkerCluster.Default.css"
import "./annuaire.scss"
import "./index.css"
import App from "./App"
import reportWebVitals from "./reportWebVitals"

import { MatomoProvider, createInstance } from "@m4tt72/matomo-tracker-react"

const instance = createInstance({
    urlBase: process.env.REACT_APP_MATOMO_URLBASE,
    siteId: process.env.REACT_APP_MATOMO_SITEID,
    //userId: 'UID76903202', // optional, default value: `undefined`.
    trackerUrl: `${process.env.REACT_APP_MATOMO_URLBASE}m`, // optional, default value: `${urlBase}matomo.php`
    srcUrl: `${process.env.REACT_APP_MATOMO_URLBASE}m.js`, // optional, default value: `${urlBase}matomo.js`
    disabled: false, // optional, false by default. Makes all tracking calls no-ops if set to true.
    heartBeat: {
        // optional, enabled by default
        active: true, // optional, default value: true
        seconds: 15 // optional, default value: `15
    },
    linkTracking: false, // optional, default value: true
    configurations: {
        // optional, default value: {}
        // any valid matomo configuration, all below are optional
        disableCookies: true,
        setSecureCookie: true,
        setRequestMethod: "POST"
    }
})

ReactDOM.render(
    <React.StrictMode>
        <MatomoProvider value={instance}>
            <App />
        </MatomoProvider>
    </React.StrictMode>,
    document.getElementById("root")
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
