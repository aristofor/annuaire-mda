import React, { useEffect, useState } from "react"
import { Link, useParams } from "react-router-dom"
import { MapContainer } from "react-leaflet"
import { TileLayer, Marker } from "react-leaflet"
import useMapConfig from "../lib/MapConfig"
import { authFetch } from "../auth"
import { useMatomo } from "@m4tt72/matomo-tracker-react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faEdit } from "@fortawesome/free-solid-svg-icons"

export default function EntryDetail() {
    const [error, setError] = useState(false)
    const [data, setData] = useState(null)
    const [tags, setTags] = useState(null)
    const mapConfig = useMapConfig()
    const { trackPageView } = useMatomo()
    const { slug } = useParams()

    useEffect(() => {
        window.scrollTo(0, 0)
        authFetch(`/api/asso/${slug}`)
            .then(r => {
                if (r.status === 404) {
                    setError(true)
                    return null
                }
                return r.json()
            })
            .then(data => {
                if (data) {
                    setData(data)
                    if (data.tags) setTags(data.tags.split(new RegExp("[ ]*,[ ]*")))
                    document.title = data.nom
                    trackPageView()
                }
            })
    }, [slug, trackPageView])

    if (error) return <h2 className="title">Not found</h2>

    if (data)
        return (
            <div className="columns">
                <div className="column is-2">
                    <figure className="image is-200x200">
                        <img src={data.avatar} alt={data.nom} />
                    </figure>
                    <br />
                    <label className="label">Contact</label>
                    {data.contact && <p>{data.contact}</p>}
                    {data.email && <p>{data.email}</p>}
                    {data.telephone && <p>{data.telephone}</p>}
                    <br />
                    <p>
                        {data.adresse}
                        <br />
                        {data.codepostal}
                        <br />
                        {data.ville}
                    </p>
                </div>
                <div className="column is-10">
                    <div className="level">
                        <div className="level-left">
                            <h1 className="title">{data.nom}</h1>
                        </div>
                        {data.editable && (
                            <div className="level-right">
                                <Link to={`/edit/${data.id}`} className="button is-link is-small">
                                    <FontAwesomeIcon icon={faEdit} />
                                </Link>
                            </div>
                        )}
                    </div>
                    <div dangerouslySetInnerHTML={{ __html: data.description }}></div>
                    {data.siteweb && (
                        <>
                            <br />
                            <p>
                                <label className="label">Site web</label>
                                <a href={data.siteweb_url} target="_blank" rel="noreferrer">
                                    {data.siteweb}
                                </a>
                            </p>
                        </>
                    )}
                    {tags && (
                        <>
                            <br />
                            <label className="label">Activités</label>
                            <p className="tags are-medium">
                                {tags.map((rep, idx) => (
                                    <span key={idx} className="tag is-link is-light">
                                        <Link to={`/activite/${encodeURIComponent(rep.toLowerCase())}`}>{rep}</Link>
                                    </span>
                                ))}
                            </p>
                        </>
                    )}
                    {data.geoloc && (
                        <>
                            <br />
                            <MapContainer center={data.geoloc} zoom={16} style={{ width: "100%", height: "260px" }}>
                                <TileLayer {...mapConfig.tileLayer} />
                                <Marker position={data.geoloc} />
                            </MapContainer>
                        </>
                    )}
                </div>
            </div>
        )

    return null
}
