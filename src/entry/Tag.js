import React, { useEffect, useState } from "react"
import { useLocation, useNavigate, useParams } from "react-router-dom"
import queryString from "query-string"
import ReactPaginate from "react-paginate"
import { authFetch } from "../auth"
import EntryListItem from "../lib/EntryListItem"

export default function Tag() {
    const [items, setItems] = useState()
    const location = useLocation()
    const navigate = useNavigate()
    const [page, setPage] = useState(1)
    const [pageCount, setPageCount] = useState(1)
    const { rep } = useParams()

    useEffect(() => {
        document.title = "Activité : " + rep
        window.scrollTo(0, 0)
        const parsed = queryString.parse(location.search)
        if (!parsed.p) parsed.p = 1
        authFetch("/api/activite/" + decodeURIComponent(rep) + "?" + queryString.stringify(parsed))
            .then(r => r.json())
            .then(data => {
                if (data) {
                    setPage(data.page)
                    setPageCount(Math.ceil(data.total / data.perPage))
                    setItems(data.items)
                }
            })
    }, [location.search, rep])

    return (
        <>
            <h1 className="title">Activité : {decodeURIComponent(rep)}</h1>
            <nav className="pagination is-small">
                <ReactPaginate
                    onPageChange={p => {
                        navigate("/activite/" + rep + "?p=" + (p.selected + 1))
                    }}
                    hrefBuilder={p => {
                        return "/activite/" + rep + "?p=" + p
                    }}
                    initialPage={page - 1}
                    forcePage={page - 1}
                    disableInitialCallback={true}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    previousLabel="<"
                    nextLabel=">"
                    containerClassName="pagination-list"
                    pageLinkClassName="pagination-link"
                    previousLinkClassName="pagination-link"
                    nextLinkClassName="pagination-link"
                    activeLinkClassName="pagination-link is-current"
                    breakLinkClassName="pagination-ellipsis"
                />
            </nav>
            {items &&
                items.map((item, idx) => {
                    return (
                        <React.Fragment key={idx}>
                            <EntryListItem {...item} />
                        </React.Fragment>
                    )
                })}
        </>
    )
}
