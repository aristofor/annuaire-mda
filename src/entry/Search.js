import React, { useEffect, useState } from "react"
import { useLocation, useNavigate } from "react-router-dom"
import queryString from "query-string"
import ReactPaginate from "react-paginate"
import { authFetch } from "../auth"
import SearchBox from "../lib/SearchBox"
import EntryListItem from "../lib/EntryListItem"
import { useMatomo } from "@m4tt72/matomo-tracker-react"

export default function Search() {
    const [items, setItems] = useState([])
    const [q, setQ] = useState("")
    const location = useLocation()
    const navigate = useNavigate()
    const [page, setPage] = useState(1)
    const [pageCount, setPageCount] = useState(1)
    const { trackSiteSearch } = useMatomo()

    useEffect(() => {
        document.title = "Recherche"
        window.scrollTo(0, 0)
        const parsed = queryString.parse(location.search)
        if (!parsed.q) return
        setQ(parsed.q)
        if (!parsed.p) parsed.p = 1
        authFetch("/api/recherche?" + queryString.stringify(parsed))
            .then(r => r.json())
            .then(data => {
                if (data) {
                    setPage(data.page)
                    setPageCount(Math.ceil(data.total / data.perPage))
                    setItems(data.items)
                    trackSiteSearch({ keyword: parsed.q, category: false, count: data.total })
                    //trackPageView()
                }
            })
        // eslint-disable-next-line
    }, [location.search])

    return (
        <>
            <div className="level">
                <div className="level-left">
                    <SearchBox q={q} />
                </div>
            </div>
            {items.length > 0 ? (
                <>
                    <nav className="pagination is-small">
                        <ReactPaginate
                            onPageChange={p => {
                                navigate("/recherche?q=" + encodeURIComponent(q) + "&p=" + (p.selected + 1))
                            }}
                            hrefBuilder={p => {
                                return "/recherche?q=" + encodeURIComponent(q) + "&p=" + p
                            }}
                            initialPage={Math.max(0, page - 1)}
                            disableInitialCallback={true}
                            pageCount={pageCount}
                            marginPagesDisplayed={2}
                            previousLabel="<"
                            nextLabel=">"
                            containerClassName="pagination-list"
                            pageLinkClassName="pagination-link"
                            previousLinkClassName="pagination-link"
                            nextLinkClassName="pagination-link"
                            activeLinkClassName="pagination-link is-current"
                            breakLinkClassName="pagination-ellipsis"
                        />
                    </nav>
                    {items.map((item, idx) => {
                        return (
                            <React.Fragment key={idx}>
                                <EntryListItem {...item} />
                            </React.Fragment>
                        )
                    })}
                </>
            ) : (
                <em>Aucun résultat</em>
            )}
        </>
    )
}
