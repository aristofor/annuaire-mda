import React, { useEffect, useState } from "react"
import { useLocation, useNavigate, useParams } from "react-router-dom"
import queryString from "query-string"
import ReactPaginate from "react-paginate"
import { authFetch } from "../auth"
import EntryListItem from "../lib/EntryListItem"

export default function Rubric() {
    const [items, setItems] = useState()
    const location = useLocation()
    const navigate = useNavigate()
    const [page, setPage] = useState(1)
    const [pageCount, setPageCount] = useState(1)
    const [label, setLabel] = useState("")
    const { id } = useParams()

    useEffect(() => {
        window.scrollTo(0, 0)
        const parsed = queryString.parse(location.search)
        if (!parsed.p) parsed.p = 1
        authFetch(`/api/rubrique/${id}?` + queryString.stringify(parsed))
            .then(r => r.json())
            .then(data => {
                if (data) {
                    setPage(data.page)
                    setPageCount(Math.ceil(data.total / data.perPage))
                    setItems(data.items)
                    setLabel(data.label)
                    document.title = "Rubrique : " + data.label
                }
            })
    }, [location.search, id])

    return (
        <>
            <h1 className="title">Rubrique : {label}</h1>
            <nav className="pagination is-small">
                <ReactPaginate
                    onPageChange={p => {
                        navigate(`/rubrique/${id}?p=` + (p.selected + 1))
                    }}
                    hrefBuilder={p => {
                        return `/rubrique/${id}?p=` + p
                    }}
                    initialPage={Math.max(0, page - 1)}
                    disableInitialCallback={true}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    previousLabel="<"
                    nextLabel=">"
                    containerClassName="pagination-list"
                    pageLinkClassName="pagination-link"
                    previousLinkClassName="pagination-link"
                    nextLinkClassName="pagination-link"
                    activeLinkClassName="pagination-link is-current"
                    breakLinkClassName="pagination-ellipsis"
                />
            </nav>
            {items &&
                items.map((item, idx) => {
                    return (
                        <React.Fragment key={idx}>
                            <EntryListItem {...item} />
                        </React.Fragment>
                    )
                })}
        </>
    )
}
