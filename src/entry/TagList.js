import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import makeColumns from "../lib/columns"

export default function TagList() {
    const COLUMNS = 3
    const [columns, setColumns] = useState([])

    useEffect(() => {
        document.title = "Activités"
        window.scrollTo(0, 0)
        fetch("/api/activites")
            .then(r => r.json())
            .then(data => {
                setColumns(makeColumns(COLUMNS, data))
            })
    }, [])

    return (
        <>
            <h1 className="title">Activités</h1>
            <div className="columns">
                {columns.map((col, idx) => {
                    return (
                        <ul key={idx} className="column">
                            {col.map((item, idx) => {
                                return (
                                    <li key={idx}>
                                        <Link to={`/activite/${encodeURIComponent(item.label)}`}>{item.label}</Link>{" "}
                                        <em className="is-size-7">{item.count}</em>
                                    </li>
                                )
                            })}
                        </ul>
                    )
                })}
            </div>
        </>
    )
}
