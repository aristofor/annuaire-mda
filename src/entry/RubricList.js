import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"

export default function RubricList() {
    const [rubrics, setRubrics] = useState()

    useEffect(() => {
        document.title = "Rubriques"
        window.scrollTo(0, 0)
        fetch("/api/rubriques")
            .then(r => r.json())
            .then(data => {
                if (data) setRubrics(data)
            })
    }, [])

    return (
        <>
            <h1 className="title">Rubriques</h1>
            {rubrics &&
                rubrics.map((item, idx) => (
                    <div key={idx} className="subtitle" style={{ margin: "0.5em 0" }}>
                        <Link to={`/rubrique/${item.id}`}>{item.label}</Link>
                        &nbsp;&nbsp;
                        <em>{item.count}</em>
                    </div>
                ))}
        </>
    )
}
