import React from "react"
import { Link, Navigate, Outlet, Route, Router, Routes, useLocation } from "react-router-dom"
import { createBrowserHistory } from "history"

import loadable from "@loadable/component"
import { useMatomo } from "@m4tt72/matomo-tracker-react"

import { useAuth, AuthContextProvider } from "./auth"

import Navigation from "./lib/Navigation"
import Home from "./main/Home"
import Apropos from "./main/Apropos"
const Confinement = loadable(() => import("./main/Confinement"))
const AideAlimentaire = loadable(() => import("./main/AideAlimentaire"))

const Login = loadable(() => import(/* webpackChunkName: "user" */ "./auth/Login"))
const Logout = loadable(() => import(/* webpackChunkName: "user" */ "./auth/Logout"))
const Account = loadable(() => import(/* webpackChunkName: "user" */ "./user/Account"))
const Register = loadable(() => import(/* webpackChunkName: "user" */ "./user/Register"))
const Confirm = loadable(() => import(/* webpackChunkName: "user" */ "./user/Confirm"))
const Recover = loadable(() => import(/* webpackChunkName: "user" */ "./user/Recover"))
const Retrieve = loadable(() => import(/* webpackChunkName: "user" */ "./user/Retrieve"))
const MyEntries = loadable(() => import(/* webpackChunkName: "user" */ "./user/MyEntries"))
const EntryEdit = loadable(() => import(/* webpackChunkName: "user" */ "./user/EntryEdit"))

const EntryDetail = loadable(() => import(/* webpackChunkName: "entry" */ "./entry/EntryDetail"))
const EntryList = loadable(() => import(/* webpackChunkName: "entry" */ "./entry/EntryList"))
const RubricList = loadable(() => import(/* webpackChunkName: "entry" */ "./entry/RubricList"))
const Rubric = loadable(() => import(/* webpackChunkName: "entry" */ "./entry/Rubric"))
const TagList = loadable(() => import(/* webpackChunkName: "entry" */ "./entry/TagList"))
const Tag = loadable(() => import(/* webpackChunkName: "entry" */ "./entry/Tag"))
const Search = loadable(() => import(/* webpackChunkName: "entry" */ "./entry/Search"))

const history = createBrowserHistory()

history.listen(location => {
    const el = document.getElementById("nav-toggle-state")
    if (el) el.checked = false
})

const PrivateOutlet = () => {
    const location = useLocation()
    const userInfo = useAuth()
    return userInfo ? <Outlet /> : <Navigate to={"/login?next=" + encodeURIComponent(location.pathname)} />
}

const CustomRouter = ({ basename, children, history }) => {
    const [state, setState] = React.useState({
        action: history.action,
        location: history.location
    })

    React.useLayoutEffect(() => history.listen(setState), [history])

    return (
        <Router
            basename={basename}
            children={children}
            location={state.location}
            navigationType={state.action}
            navigator={history}
        />
    )
}

function App() {
    const { enableLinkTracking } = useMatomo()
    enableLinkTracking()

    return (
        <CustomRouter history={history}>
            <AuthContextProvider>
                <Navigation />
                <section className="section main-content">
                    <div className="container">
                        <Routes>
                            <Route exact path="/" element={<Home />} />
                            <Route exact path="/login" element={<Login />} />
                            <Route exact path="/apropos" element={<Apropos />} />
                            <Route exact path="/register" element={<Register />} />
                            <Route exact path="/confirm/:token" element={<Confirm />} />
                            <Route exact path="/recover" element={<Recover />} />
                            <Route exact path="/retrieve/:token" element={<Retrieve />} />
                            <Route exact path="/confinement" element={<Confinement />} />
                            <Route exact path="/aidealimentaire" element={<AideAlimentaire />} />
                            <Route exact path="/asso/:slug" element={<EntryDetail />} />
                            <Route exact path="/associations" element={<EntryList />} />
                            <Route exact path="/rubriques" element={<RubricList />} />
                            <Route exact path="/rubrique/:id" element={<Rubric />} />
                            <Route exact path="/activites" element={<TagList />} />
                            <Route exact path="/activite/:rep" element={<Tag />} />
                            <Route exact path="/recherche" element={<Search />} />
                            <Route path="/" element={<PrivateOutlet />}>
                                <Route exact path="/logout" element={<Logout />} />
                                <Route exact path="/account" element={<Account />} />
                                <Route exact path="/mes_entrees" element={<MyEntries />} />
                                <Route exact path="/edit/:id" element={<EntryEdit />} />
                                <Route exact path="/ajouter" element={<EntryEdit />} />
                            </Route>
                            {/* catchall */}
                            <Route path="*" element={<h2 className="title">Not found</h2>} />
                        </Routes>
                    </div>
                </section>
                <div className="container">
                    <footer className="footer">
                        <div className="level">
                            <div className="level-left">
                                <img src="/static/img/logo_mda.png" width="40" height="61" alt="MDA" />
                            </div>
                            <div className="level-item">
                                <Link to="/apropos">À propos</Link>
                            </div>
                            <div className="level-right">
                                <img src="/static/img/blank.png" width="40" height="40" alt="" />
                            </div>
                        </div>
                    </footer>
                </div>
            </AuthContextProvider>
        </CustomRouter>
    )
}

export default App
