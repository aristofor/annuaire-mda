import React, { useEffect, useState } from "react"

export default function Recover() {
    const [email, setEmail] = useState("")
    const [message, setMessage] = useState("")
    const [done, setDone] = useState(false)

    useEffect(() => {
        document.title = "Récupération"
    }, [])

    const handleSubmit = evt => {
        evt.preventDefault()
        fetch("/api/recover", { method: "POST", body: JSON.stringify({ email: email }) })
            .then(r => r.json())
            .then(data => {
                if (data.message) setMessage(data.message)
                if (data.done) setDone(true)
            })
    }

    return done ? (
        <>
            <h1 className="title">Récupération de compte</h1>
            <p>Un lien de récupération a été envoyé à {email}</p>
        </>
    ) : (
        <div className="column is-4 is-offset-4">
            <div className="box">
                <h1 className="title">Récupération de compte</h1>
                {message && <div className="notification is-danger">{message}</div>}
                <form onSubmit={handleSubmit}>
                    <div className="field">
                        <label className="label" htmlFor="email">
                            Adresse e-mail
                        </label>
                        <input
                            className="input"
                            type="text"
                            name="email"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                        />
                    </div>
                    <br />
                    <div className="field is-grouped">
                        <div className="control">
                            <button type="submit" className="button is-primary">
                                Valider
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    )
}
