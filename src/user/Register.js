import React, { useEffect, useState } from "react"
import CloseOnEscape from "react-close-on-escape"

const Modal = ({ closeModal, modalState }) => {
    const [html, setHtml] = useState("")

    useEffect(() => {
        fetch("/api/cgu")
            .then(r => r.json())
            .then(data => {
                setHtml(data.html)
            })
    }, [])

    if (!modalState) return null

    return (
        <div className="modal is-active">
            <div className="modal-background" onClick={closeModal}></div>
            <div className="modal-card" style={{ width: "60%" }}>
                <header className="modal-card-head">
                    <button className="delete" aria-label="close" onClick={closeModal}></button>
                </header>
                <section className="modal-card-body">
                    <div className="content" dangerouslySetInnerHTML={{ __html: html }}></div>
                </section>
                <footer className="modal-card-foot">
                    <button className="button" onClick={closeModal}>
                        Fermer
                    </button>
                </footer>
            </div>
        </div>
    )
}

export default function Register() {
    const [info, setInfo] = useState({
        email: "",
        password1: "",
        password2: "",
        cgu: ""
    })
    const [message, setMessage] = useState("")
    const [done, setDone] = useState(false)

    const [modalState, setModalState] = useState(false)

    useEffect(() => {
        document.title = "Inscription"
    }, [])

    const handleClose = evt => {
        setModalState(false)
        if (evt) evt.preventDefault()
    }

    const onChange = evt => {
        setInfo({ ...info, [evt.target.name]: evt.target.value })
    }

    const handleSubmit = evt => {
        evt.preventDefault()
        if (info.password1 !== info.password2) {
            setMessage("Les mots de passe ne correspondent pas.")
            return
        }
        if (info.password1.length < 5) {
            setMessage("Le mot de passe est trop court.")
            return
        }
        if (!info.cgu) {
            setMessage("Vous devez accepter les CGU.")
            return
        }
        setMessage("")
        fetch("/api/register", {
            method: "POST",
            body: JSON.stringify({ email: info.email, password: info.password1 })
        })
            .then(r => r.json())
            .then(data => {
                if (data.message) setMessage(data.message)
                if (data.done) setDone(true)
            })
    }

    return done ? (
        <div className="content">
            <h2 className="subtitle">Inscription en cours</h2>
            <p>Vous allez recevoir un lien de confirmation à l'adresse {info.email}.</p>
        </div>
    ) : (
        <>
            <div className="column is-4 is-offset-4">
                <div className="box">
                    <h1 className="title">Inscription</h1>
                    {message && <div className="notification is-danger">{message}</div>}
                    <form onSubmit={handleSubmit}>
                        <div className="field">
                            <label className="label" htmlFor="email">
                                Adresse e-mail
                            </label>
                            <input className="input" type="text" name="email" value={info.email} onChange={onChange} />
                        </div>
                        <div className="field">
                            <label className="label" htmlFor="password1">
                                Mot de passe
                            </label>
                            <input
                                className="input"
                                type="password"
                                name="password1"
                                value={info.password1}
                                onChange={onChange}
                                autoComplete="off"
                            />
                        </div>
                        <div className="field">
                            <label className="label" htmlFor="password2">
                                Confirmation
                            </label>
                            <input
                                className="input"
                                type="password"
                                name="password2"
                                value={info.password2}
                                onChange={onChange}
                                autoComplete="off"
                            />
                        </div>
                        <div className="field">
                            <label className="checkbox">
                                <input type="checkbox" name="cgu" id="cgu" value="y" onChange={onChange} />
                                <label htmlFor="cgu">
                                    &nbsp;&nbsp; J'accepte les{" "}
                                    <a
                                        href="/#"
                                        onClick={e => {
                                            e.preventDefault()
                                            setModalState(true)
                                        }}>
                                        conditions générales d'utilisation
                                    </a>
                                    .
                                </label>
                            </label>
                        </div>
                        <br />
                        <div className="field is-grouped">
                            <div className="control">
                                <button type="submit" className="button is-primary">
                                    Valider
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <CloseOnEscape onEscape={handleClose}>
                <Modal modalState={modalState} closeModal={handleClose} />
            </CloseOnEscape>
        </>
    )
}
