import React, { useContext, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { login, AuthContext } from "../auth"

export default function Retrieve() {
    const [message, setMessage] = useState("")
    const authContext = useContext(AuthContext)
    const navigate = useNavigate()
    const { token } = useParams()

    useEffect(() => {
        document.title = "Récupération"
        fetch(`/api/retrieve/${token}`)
            .then(r => r.json())
            .then(data => {
                if (data.message) setMessage(data.message)
                if (data.access_token) {
                    login({ access_token: data.access_token })
                    authContext.invalidateUserInfo()
                    navigate("/account")
                }
            })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [authContext, token])

    return <>{message && <div className="notification is-danger">{message}</div>}</>
}
