import React, { useEffect, useState } from "react"
import { useLocation, useNavigate } from "react-router-dom"
import queryString from "query-string"
import ReactPaginate from "react-paginate"
import CloseOnEscape from "react-close-on-escape"
import { authFetch } from "../auth"
import EntryListItem from "../lib/EntryListItem"

const DeleteModal = ({ closeModal, onOk, modalState, target }) => {
    const handleSubmit = evt => {
        evt.preventDefault()
        onOk(target.id)
        closeModal()
    }

    if (!modalState) return null

    return (
        <CloseOnEscape onEscape={closeModal}>
            <div className="modal is-active">
                <form onSubmit={handleSubmit}>
                    <div className="modal-background" onClick={closeModal}></div>
                    <div className="modal-card">
                        <header className="modal-card-head">
                            <span className="modal-card-title">Confirmez l'effacement de l'entrée</span>
                            <button className="delete" aria-label="close" onClick={closeModal}></button>
                        </header>
                        <section className="modal-card-body">
                            <p>{target.nom}</p>
                        </section>
                        <footer className="modal-card-foot">
                            <button type="submit" className="button is-primary">
                                Valider
                            </button>
                            <button type="button" className="button" onClick={closeModal}>
                                Annuler
                            </button>
                        </footer>
                    </div>
                </form>
            </div>
        </CloseOnEscape>
    )
}

export default function MyEntries() {
    const [items, setItems] = useState()
    const location = useLocation()
    const navigate = useNavigate()
    const [page, setPage] = useState(1)
    const [pageCount, setPageCount] = useState(0)
    const [deleteModalState, setDeleteModalState] = useState()
    const [modalTarget, setModalTarget] = useState()

    useEffect(() => {
        document.title = "Mes entrées"
        window.scrollTo(0, 0)
        const parsed = queryString.parse(location.search)
        if (!parsed.p) parsed.p = 1
        authFetch("/api/mes_entrees?" + queryString.stringify(parsed))
            .then(r => r.json())
            .then(data => {
                if (data) {
                    setPage(data.page)
                    setPageCount(Math.ceil(data.total / data.perPage))
                    setItems(data.items)
                }
            })
    }, [location.search])

    const onDelete = ({ id, nom }) => {
        setModalTarget({ id: id, nom: nom })
        setDeleteModalState(true)
    }

    const doDelete = id => {
        authFetch(`/api/edit/${id}`, { method: "DELETE" })
            .then(r => r.json())
            .then(data => {
                navigate(0)
            })
    }

    return (
        <>
            <h1 className="title">Mes entrées</h1>
            <nav className="pagination is-small">
                <ReactPaginate
                    onPageChange={p => {
                        navigate("/mes_entrees?p=" + (p.selected + 1))
                    }}
                    hrefBuilder={p => {
                        return "/mes_entrees?p=" + p
                    }}
                    initialPage={Math.max(0, page - 1)}
                    disableInitialCallback={true}
                    pageCount={pageCount}
                    marginPagesDisplayed={2}
                    previousLabel="<"
                    nextLabel=">"
                    containerClassName="pagination-list"
                    pageLinkClassName="pagination-link"
                    previousLinkClassName="pagination-link"
                    nextLinkClassName="pagination-link"
                    activeLinkClassName="pagination-link is-current"
                    breakLinkClassName="pagination-ellipsis"
                />
            </nav>
            {items &&
                items.map((item, idx) => {
                    return (
                        <React.Fragment key={idx}>
                            <EntryListItem onDelete={onDelete} {...item} />
                        </React.Fragment>
                    )
                })}
            <DeleteModal
                modalState={deleteModalState}
                target={modalTarget}
                onOk={doDelete}
                closeModal={_ => setDeleteModalState(false)}
            />
        </>
    )
}
