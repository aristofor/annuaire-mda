import React, { useContext, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { login, AuthContext } from "../auth"

export default function Confirm() {
    const [done, setDone] = useState(false)
    const [message, setMessage] = useState("")
    const authContext = useContext(AuthContext)
    const { token } = useParams()

    useEffect(() => {
        document.title = "Confirmation"
        fetch(`/api/confirm/${token}`)
            .then(r => r.json())
            .then(data => {
                if (data.message) setMessage(data.message)
                if (data.done) setDone(true)
                if (data.access_token) {
                    login({ access_token: data.access_token })
                    authContext.invalidateUserInfo()
                }
            })
    }, [authContext, token])

    return done ? (
        <h1 className="title">Inscription terminée.</h1>
    ) : (
        <>{message && <div className="notification is-danger">{message}</div>}</>
    )
}
