import React, { useEffect, useRef, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { TileLayer, Marker, MapContainer } from "react-leaflet"
import useMapConfig from "../lib/MapConfig"
import { authFetch } from "../auth"
import useRubrics from "../lib/Rubrics"
import TagsInput from "../lib/TagsInput"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faUpload } from "@fortawesome/free-solid-svg-icons"

export default function EntryEdit() {
    const [formData, setFormData] = useState({})
    const [initData, setInitData] = useState({})
    const [message, setMessage] = useState("")
    const [errors, setErrors] = useState({})
    const rubrics = useRubrics()
    const form = useRef(null)
    const mapConfig = useMapConfig()
    const navigate = useNavigate()
    const timer = useRef(null)
    const [geoloc, setGeoloc] = useState(null)
    const [map, setMap] = useState(null)
    const [tags, setTags] = useState([])
    const { id } = useParams()

    /* zooms pour la carte sans/avec geoloc */
    const ZOOM_DEFAULT = 11
    const ZOOM_GEOLOC = 14

    useEffect(() => {
        document.title = "Édition"
        window.scrollTo(0, 0)
        if (id)
            authFetch(`/api/edit/${id}`)
                .then(r => r.json())
                .then(data => {
                    if (data) {
                        setFormData(data)
                        setInitData(data)
                        if (data.geoloc_ok) {
                            setGeoloc({ lat: data.geoloc_lat, lng: data.geoloc_lng })
                        } else {
                            setGeoloc(null)
                        }
                        if (data.tags) setTags(data.tags.split(new RegExp("[ ]*,[ ]*")))
                    }
                })
        else {
            setFormData({})
            setInitData({})
            setGeoloc(null)
            setTags([])
        }
        return () => clearTimeout(timer.current)
    }, [id])

    const handleSubmit = evt => {
        evt.preventDefault()
        setMessage("")
        setErrors({})
        const postData = new FormData(form.current)
        /*
        // option: ajouter les checkboxes fausses
        const cbs = form.current.querySelectorAll("input[type=checkbox]")
        cbs.forEach(field => {
            if (!(field.name in postData) && !field.checked) postData.append(field.name, "")
        })
        */
        postData.append("tags", tags.join(","))
        if (id)
            authFetch(`/api/edit/${id}`, { method: "PUT", body: postData })
                .then(r => r.json())
                .then(data => {
                    if (data) {
                        if (data.message) setMessage(data.message)
                        if (data.errors) setErrors(data.errors)
                        if (!(data.message || data.errors)) navigate("/asso/" + data.slug)
                    }
                })
        else
            authFetch("/api/edit", { method: "POST", body: postData })
                .then(r => r.json())
                .then(data => {
                    if (data) {
                        if (data.message) setMessage(data.message)
                        if (data.errors) setErrors(data.errors)
                        if (!(data.message || data.errors)) navigate("/asso/" + data.slug)
                    }
                })
    }

    const handleChange = evt => {
        let value
        if (evt.target.type === "checkbox") value = evt.target.checked
        else value = evt.target.value
        setFormData({ ...formData, [evt.target.name]: value })
    }

    const handleReset = evt => {
        evt.preventDefault()
        setFormData(initData)
        if (initData.geoloc_ok) {
            setGeoloc({ lat: initData.geoloc_lat, lng: initData.geoloc_lng })
        } else setGeoloc(null)
    }

    const handleChangeAdresse = evt => {
        setFormData({ ...formData, [evt.target.name]: evt.target.value })
        clearTimeout(timer.current)
        timer.current = setTimeout(cbChangeAdresse, 1200)
    }

    const cbChangeAdresse = () => {
        fetch(
            "https://api-adresse.data.gouv.fr/search/?q=" +
                encodeURIComponent(formData.adresse + "," + formData.ville) +
                +"&lat=" +
                mapConfig.center[0] +
                "&lon" +
                mapConfig.center[1] +
                (formData.codepostal ? "&postcode=" + encodeURIComponent(formData.codepostal) : "")
        )
            .then(r => r.json())
            .then(data => {
                const features = data.features
                if (features.length) {
                    const result = features[0]
                    //console.log(result.properties.score)
                    const coord = result.geometry.coordinates
                    setGeoloc({ lat: coord[1], lng: coord[0] })
                    map.setView([coord[1], coord[0]], ZOOM_GEOLOC)
                } else setGeoloc(null)
            })
    }

    return (
        <>
            {message && <div className="notification is-danger">{message}</div>}
            <div className="box">
                <h1 className="title">{id ? "Édition" : "Nouvelle entrée"}</h1>
                <form ref={form} onSubmit={handleSubmit} onReset={handleReset}>
                    <div className="field">
                        <input
                            id="active"
                            type="checkbox"
                            name="active"
                            className="switch"
                            checked={formData.active ? "checked" : ""}
                            onChange={handleChange}
                        />
                        <label htmlFor="active" className="label">
                            Association active
                        </label>
                    </div>
                    <input type="hidden" name="geoloc_ok" value={geoloc ? 1 : ""} />
                    <input type="hidden" name="geoloc_lat" value={(geoloc && geoloc.lat) || ""} />
                    <input type="hidden" name="geoloc_lng" value={(geoloc && geoloc.lng) || ""} />
                    <div className="columns">
                        {/* Colonne gauche */}
                        <div className="column is-two-thirds">
                            {/*
                            <div className="field">
                                <label className="checkbox">
                                    <input
                                        type="checkbox"
                                        id="confinement"
                                        name="confinement"
                                        value="y"
                                        onChange={handleChange}
                                        checked={formData.confinement || false}
                                    />
                                    &nbsp;&nbsp;
                                    <label htmlFor="confinement">Association active pendant le confinement</label>
                                </label>
                            </div>
                            */}
                            {/*
                            <div className="field">
                                <label className="checkbox">
                                    <input
                                        type="checkbox"
                                        id="ukraine"
                                        name="ukraine"
                                        value="y"
                                        onChange={handleChange}
                                        checked={formData.ukraine || false}
                                    />
                                    &nbsp;&nbsp;
                                    <label htmlFor="ukraine">Solidarité Ukraine</label>
                                </label>
                            </div>
                            */}
                            <div className="field">
                                <label className="label" htmlFor="nom">
                                    Nom complet
                                </label>
                                <input
                                    className={`input is-large${errors.nom ? " is-danger" : ""}`}
                                    type="text"
                                    id="nom"
                                    name="nom"
                                    onChange={handleChange}
                                    value={formData.nom || ""}
                                />
                                {errors.nom && (
                                    <div className="is-size-7 message has-text-danger is-danger">{errors.nom}</div>
                                )}
                            </div>
                            <div className="field">
                                <label className="label" htmlFor="description">
                                    Description
                                </label>
                                <textarea
                                    className={`textarea${errors.description ? " is-danger" : ""}`}
                                    id="description"
                                    name="description"
                                    rows="8"
                                    onChange={handleChange}
                                    value={formData.description || ""}></textarea>
                                {errors.description && (
                                    <div className="is-size-7 message has-text-danger is-danger">
                                        {errors.description}
                                    </div>
                                )}
                            </div>
                            <div className="field">
                                <label className="label" htmlFor="siteweb">
                                    Site web
                                </label>
                                <input
                                    className="input"
                                    type="text"
                                    id="siteweb"
                                    name="siteweb"
                                    onChange={handleChange}
                                    value={formData.siteweb || ""}
                                />
                            </div>
                            <div className="field">
                                <label className="label">Activités</label>
                                <TagsInput tags={tags} onChange={setTags} inputProps={{ placeholder: "activité" }} />
                            </div>
                        </div>
                        {/* Colonne droite */}
                        <div className="column is-one-third">
                            <div className="field">
                                <label className="label" htmlFor="rubrique">
                                    Rubrique
                                </label>
                                <div className="select">
                                    <select
                                        id="rubrique"
                                        name="rubrique"
                                        onChange={handleChange}
                                        value={formData.rubrique}>
                                        {rubrics &&
                                            rubrics.map(item => (
                                                <option key={item.id} value={item.id}>
                                                    {item.label}
                                                </option>
                                            ))}
                                    </select>
                                </div>
                            </div>
                            <div className="file">
                                <label className="file-label">
                                    <input className="file-input" type="file" name="image" onChange={handleChange} />
                                    <span className="file-cta">
                                        <span className="file-icon">
                                            <FontAwesomeIcon icon={faUpload} />
                                        </span>
                                        <span className="file-label">Fichier…</span>
                                    </span>
                                    <span className="file-name is-hidden"></span>
                                </label>
                            </div>
                            <br />
                            <figure className="image is-200x200">
                                <img src={formData.avatar} alt="" />
                            </figure>
                            <br />
                            <div className="field">
                                <label className="label" htmlFor="contact">
                                    Personne à contacter
                                </label>
                                <input
                                    className="input"
                                    type="text"
                                    id="contact"
                                    name="contact"
                                    onChange={handleChange}
                                    value={formData.contact || ""}
                                />
                            </div>
                            <div className="field">
                                <label className="label" htmlFor="email">
                                    Adresse e-mail
                                </label>
                                <input
                                    className="input"
                                    type="text"
                                    id="email"
                                    name="email"
                                    onChange={handleChange}
                                    value={formData.email || ""}
                                />
                            </div>
                            <div className="field">
                                <label className="label" htmlFor="telephone">
                                    Téléphone
                                </label>
                                <input
                                    className="input"
                                    type="text"
                                    id="telephone"
                                    name="telephone"
                                    onChange={handleChange}
                                    value={formData.telephone || ""}
                                />
                            </div>
                        </div>
                    </div>
                    {/* 2e ligne */}
                    <div className="columns">
                        <div className="column is-two-thrids">
                            <div className="field">
                                <label className="label" htmlFor="adresse">
                                    Adresse
                                </label>
                                <input
                                    className={`input${errors.adresse ? " is-danger" : ""}`}
                                    type="text"
                                    name="adresse"
                                    onChange={handleChangeAdresse}
                                    value={formData.adresse || ""}
                                />
                                {errors.adresse && (
                                    <div className="is-size-7 message has-text-danger is-danger">{errors.adresse}</div>
                                )}
                            </div>
                            <div className="columns">
                                <div className="column is-one-quarter">
                                    <div className="field">
                                        <label className="label" htmlFor="codepostal">
                                            Code postal
                                        </label>
                                        <input
                                            className={`input${errors.codepostal ? " is-danger" : ""}`}
                                            type="text"
                                            name="codepostal"
                                            onChange={handleChangeAdresse}
                                            value={formData.codepostal || ""}
                                        />
                                        {errors.codepostal && (
                                            <div className="is-size-7 message has-text-danger is-danger">
                                                {errors.codepostal}
                                            </div>
                                        )}
                                    </div>
                                </div>
                                <div className="column">
                                    <div className="field">
                                        <label className="label" htmlFor="ville">
                                            Ville
                                        </label>
                                        <input
                                            className={`input${errors.ville ? " is-danger" : ""}`}
                                            type="text"
                                            name="ville"
                                            onChange={handleChangeAdresse}
                                            value={formData.ville || ""}
                                        />
                                        {errors.ville && (
                                            <div className="is-size-7 message has-text-danger is-danger">
                                                {errors.ville}
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="column is-one-third">
                            {mapConfig && (
                                <MapContainer
                                    whenCreated={map => setMap(map)}
                                    center={geoloc ? [geoloc.lat, geoloc.lng] : mapConfig.center}
                                    zoom={geoloc ? ZOOM_GEOLOC : ZOOM_DEFAULT}
                                    style={{ width: "100%", height: "180px" }}>
                                    <TileLayer {...mapConfig.tileLayer} />
                                    {geoloc && <Marker position={[geoloc.lat, geoloc.lng]} />}
                                </MapContainer>
                            )}
                        </div>
                    </div>
                    {/* Boutons */}
                    <div className="field is-grouped">
                        <div className="control">
                            <button type="submit" className="button is-primary">
                                Valider
                            </button>
                        </div>
                        &nbsp;
                        <div className="control">
                            <button type="reset" className="button is-default">
                                Annuler
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </>
    )
}
