import React, { useContext, useEffect, useState } from "react"
import { AuthContext, authFetch } from "../auth"

export default function Account() {
    const authContext = useContext(AuthContext)
    const [password1, setPassword1] = useState("")
    const [password2, setPassword2] = useState("")
    const [error, setError] = useState(false)
    const [message, setMessage] = useState("")

    useEffect(() => {
        document.title = "Mon compte"
    }, [])

    const handleSubmit = evt => {
        evt.preventDefault()
        if (password1 !== password2) {
            setMessage("Les mots de passe ne correspondent pas.")
            setError(true)
            return
        }
        if (password1.length < 5) {
            setMessage("Le mot de passe est trop court.")
            setError(true)
            return
        }
        setMessage("")
        setError(false)
        const requestOptions = {
            method: "POST",
            body: JSON.stringify({ password: password1 })
        }
        authFetch("/api/password", requestOptions)
            .then(r => r.json())
            .then(data => {
                if (data.message) setMessage(data.message)
                setPassword1("")
                setPassword2("")
            })
            .catch(error => {
                console.log(error)
            })
    }

    return (
        <section className="hero is-halfheight">
            <div className="hero-body">
                <div className="container">
                    <div className="columns">
                        <div className="column is-offset-4 is-4">
                            <div className="box">
                                <h1 className="title">Mon compte</h1>
                                <form onSubmit={handleSubmit}>
                                    <div className="field">
                                        <label className="label">E-mail</label>
                                        <div className="control">
                                            <input
                                                disabled="disabled"
                                                type="text"
                                                className="input"
                                                defaultValue={authContext.userInfo.username}
                                            />
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label className="label">Nouveau mot de passe</label>
                                        <div className="control">
                                            <input
                                                autoFocus
                                                type="password"
                                                className="input"
                                                value={password1}
                                                onChange={e => {
                                                    setPassword1(e.target.value)
                                                }}
                                                autoComplete="off"
                                            />
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label className="label">Confirmation</label>
                                        <div className="control">
                                            <input
                                                type="password"
                                                className="input"
                                                value={password2}
                                                onChange={e => {
                                                    setPassword2(e.target.value)
                                                }}
                                                autoComplete="off"
                                            />
                                        </div>
                                    </div>
                                    <br />
                                    <div className="field">
                                        <div className="control">
                                            <input type="submit" value="Submit" className="button is-primary" />
                                        </div>
                                    </div>
                                </form>
                                {message && (
                                    <>
                                        <br />
                                        <div className={`notification ${error ? "is-danger" : "is-success"}`}>
                                            {message}
                                        </div>
                                    </>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}
