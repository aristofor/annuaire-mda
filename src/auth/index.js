import React, { useState, useEffect } from "react"
import { createAuthProvider } from "react-token-auth"
import jwt_decode from "jwt-decode"

export const localStorageKey = "ANNUAIRE_MDA_TOKEN_AUTH"

export const [useAuth, authFetch, login, logout] = createAuthProvider({
    accessTokenKey: "access_token",
    localStorageKey: localStorageKey,
    onUpdateToken: token =>
        fetch("/api/refresh", {
            method: "POST",
            headers: { Authorization: "Bearer " + token.refresh_token }
        }).then(r => r.json())
})

export const AuthContext = React.createContext()

export function AuthContextProvider({ children }) {
    const [userInfo, setUserInfo] = useState(null)

    const invalidateUserInfo = () => {
        const raw_data = window.localStorage.getItem(localStorageKey)
        try {
            const token = JSON.parse(raw_data).access_token
            const jwt_payload = jwt_decode(token)
            setUserInfo({
                username: jwt_payload.username ? jwt_payload.username : "#" + jwt_payload.id
            })
        } catch (error) {
            setUserInfo(null)
            logout()
        }
    }

    useEffect(() => {
        invalidateUserInfo()
    }, [])

    const defaultContext = {
        userInfo,
        setUserInfo,
        invalidateUserInfo
    }

    return <AuthContext.Provider value={defaultContext}>{children}</AuthContext.Provider>
}
