import React, { useEffect, useContext } from "react"
import { Navigate } from "react-router-dom"
import { logout, AuthContext } from "./index"

export default function Logout() {
    const authContext = useContext(AuthContext)

    useEffect(() => {
        logout()
        authContext.invalidateUserInfo()
    }, [authContext])

    return <Navigate to="/" replace={true} />
}
