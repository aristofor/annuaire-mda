import React, { useState, useContext, useEffect } from "react"
import { Link, useLocation, useNavigate } from "react-router-dom"
import { login, useAuth, logout, AuthContext } from "./index"

export default function Login() {
    const [credentials, setCredentials] = useState({
        username: "",
        password: ""
    })

    const authContext = useContext(AuthContext)
    const [logged] = useAuth()
    const [message, setMessage] = useState("")

    const location = useLocation()
    const navigate = useNavigate()

    const onChange = evt => {
        setCredentials({ ...credentials, [evt.target.name]: evt.target.value })
    }

    useEffect(() => {
        document.title = "Connexion"
    }, [])

    const handleSubmit = evt => {
        evt.preventDefault()
        fetch("/api/login", {
            method: "POST",
            body: JSON.stringify(credentials)
        })
            .then(r => r.json())
            .then(data => {
                if (data.access_token) {
                    login(data)
                    authContext.invalidateUserInfo()
                    const params = new URLSearchParams(location.search)
                    const next = params.get("next")
                    navigate(next ? next : "/")
                } else {
                    setMessage("Identifiant ou mot de passe incorrect.")
                }
            })
    }

    const handleReauthenticate = evt => {
        evt.preventDefault()
        authContext.setUserInfo(null)
        logout()
    }

    return (
        <div className="hero is-small">
            <div className="hero-body">
                <div className="columns">
                    <div className="column is-offset-4 is-4">
                        <div className="box">
                            <h1 className="title">Connexion</h1>
                            {message && <div className="notification is-danger">{message}</div>}
                            {logged ? (
                                <button onClick={handleReauthenticate} type="submit" className="button is-primary">
                                    Me réauthentifier
                                </button>
                            ) : (
                                <form onSubmit={handleSubmit}>
                                    <div className="field">
                                        <label className="label" htmlFor="username">
                                            Adresse e-mail
                                        </label>
                                        <div className="control">
                                            <input
                                                id="username"
                                                name="username"
                                                className="input"
                                                type="text"
                                                placeholder="Utilisateur"
                                                onChange={onChange}
                                                value={credentials.username}
                                            />
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label className="label" htmlFor="password">
                                            Mot de passe
                                        </label>
                                        <div className="control">
                                            <input
                                                id="password"
                                                name="password"
                                                className="input"
                                                type="password"
                                                placeholder="Mot de passe"
                                                onChange={onChange}
                                                value={credentials.password}
                                                autoComplete="off"
                                            />
                                        </div>
                                    </div>
                                    <br />
                                    <div className="field">
                                        <div className="control">
                                            <div className="level">
                                                <button type="submit" className="button is-primary">
                                                    Connexion
                                                </button>
                                                <p>
                                                    <Link to="/recover">mot de passe oublié ?</Link>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            )}
                        </div>
                        {!logged && (
                            <div className="content">
                                <p>
                                    <br />
                                    Vous voulez référencer votre association ?
                                    <br />
                                    <br />
                                    <Link className="button is-default" to="/register">
                                        Créer un compte
                                    </Link>
                                    <br />
                                </p>
                                <p>
                                    Vous êtes perdu ? Vous pouvez consulter ce{" "}
                                    <a href="/static/img/tuto-carto-confinement.pdf" target="_blank">
                                        tuto
                                    </a>
                                    .
                                </p>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </div>
    )
}
