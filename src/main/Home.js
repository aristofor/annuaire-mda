import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { MapContainer } from "react-leaflet"
import SearchBox from "../lib/SearchBox"
import MapComponent from "../lib/MapComponent"
import { useMatomo } from "@m4tt72/matomo-tracker-react"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faRssSquare } from "@fortawesome/free-solid-svg-icons"

export default function Home() {
    const [banner, setBanner] = useState(null)
    const [recent, setRecent] = useState([])
    const { trackPageView } = useMatomo()

    useEffect(() => {
        document.title = "Annuaire des associations"
        window.scrollTo(0, 0)
        fetch("/api/home")
            .then(r => r.json())
            .then(data => {
                if (data.banner) setBanner(data.banner)
                if (data.recent) setRecent(data.recent)
            })
        trackPageView()
    }, [trackPageView])

    return (
        <>
            {/*
            {banner && (
                <div style={{ width: "100%", height: "600px", position: "absolute", zIndex: "-1" }}>
                    <img src={`/static/banner/${banner}`} alt="banner" width="1344" height="600" />
                </div>
            )}
            */}
            <div
                style={{
                    width: "100%",
                    height: "600px",
                    background: banner ? `center url("/static/banner/${banner}")` : ""
                }}>
                <br />
                <div className="level">
                    <div className="level-item" style={{ margin: "1.5em" }}>
                        <div className="box" style={{ padding: "1em", backgroundColor: "rgba(255,255,255,0.6)" }}>
                            <div className="subtitle">RECHERCHEZ LES ASSOCIATIONS ET COLLECTIFS PRÈS DE CHEZ VOUS</div>
                            <br />
                            <div className="level">
                                <div className="level-item">
                                    <SearchBox />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style={{ marginTop: "-2em" }}>
                <div className="level">
                    <div className="level-item">
                        <Link className="button is-primary is-large" to="/associations">
                            ASSOCIATIONS
                        </Link>
                    </div>
                    <div className="level-item">
                        <Link className="button is-primary is-large" to="/rubriques">
                            RUBRIQUES
                        </Link>
                    </div>
                    <div className="level-item">
                        <Link className="button is-primary is-large" to="/activites">
                            ACTIVITÉS
                        </Link>
                    </div>
                    {/*
                    <div className="level-item">
                        <Link className="button is-primary is-large" to="/ukraine">
                            UKRAINE
                        </Link>
                    </div>
                    */}
                    {/*
                    <div className="level-item">
                        <Link className="button is-primary is-large" to="/confinement">
                            CONFINEMENT
                        </Link>
                    </div>
                    */}
                </div>
            </div>
            <br />
            <br />
            <div className="columns">
                <div className="column">
                    {recent.length > 0 &&
                        recent.map((item, idx) => {
                            return (
                                <React.Fragment key={idx}>
                                    <div className="card">
                                        <div className="card-content">
                                            <div className="image is-75x75">
                                                <img src={item.avatar} alt="avatar" />
                                            </div>
                                            <br />
                                            <div className="content">
                                                <Link to={`/asso/${item.slug}`}>{item.nom}</Link>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                </React.Fragment>
                            )
                        })}
                    <a href="/rss">
                        <FontAwesomeIcon icon={faRssSquare} />
                    </a>
                </div>
                <div className="column is-four-fifths">
                    <MapContainer center={[0, 0]} zoom={13} maxZoom={18} style={{ width: "100%", height: "600px" }}>
                        <MapComponent dataUrl="/api/markers" />
                    </MapContainer>
                </div>
            </div>
        </>
    )
}
