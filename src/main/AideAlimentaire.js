import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { MapContainer, Marker, Popup, TileLayer, useMap } from "react-leaflet"
import useMapConfig from "../lib/MapConfig"

function MapComponent({ dataUrl }) {
    const map = useMap()
    const mapConfig = useMapConfig()
    const [markers, setMarkers] = useState([])

    useEffect(() => {
        document.title = "Aide alimentaire"
        if (!mapConfig) return
        map.setView(mapConfig.center)
        fetch(dataUrl)
            .then(r => r.json())
            .then(data => {
                if (data) setMarkers(data.records)
            })
    }, [map, mapConfig, dataUrl])

    return !mapConfig ? null : (
        <>
            <TileLayer {...mapConfig.tileLayer} />
            {markers.map((item, idx) => (
                <Marker key={idx} id={item[0]} position={[item.geometry.coordinates[1], item.geometry.coordinates[0]]}>
                    <Popup closeButton={false}>
                        <article className="media">
                            <div className="media-content" style={{ minWidth: "200px", marginRight: "24px" }}>
                                <p className="subtitle">{item.fields.nom_site_principal}</p>
                                <p className="content">
                                    {item.fields.adresse_postale_adr}
                                    <br />
                                    {item.fields.adresse_postale_comm}
                                </p>
                                <p className="content" dangerouslySetInnerHTML={{ __html: item.fields.descriptif }}></p>
                            </div>
                        </article>
                    </Popup>
                </Marker>
            ))}
        </>
    )
}

export default function AideAlimentaire() {
    return (
        <div className="container">
            <div className="level">
                <div className="level-left">
                    <Link to="/confinement">Associations actives pendant le confinement</Link>
                </div>
                <div className="level-right">
                    <div className="title">Aide alimentaire</div>
                </div>
            </div>
            <MapContainer
                center={[0, 0]}
                zoom={12}
                maxZoom={18}
                style={{ width: "100%", height: "600px", zIndex: "1" }}>
                <MapComponent dataUrl="https://data.rennesmetropole.fr/api/records/1.0/search/?rows=999&dataset=sites_organismes_organismes&q=8.2.9&facet=nom_theme_principal&facet=nom_activite_principale&facet=nom_specialite_principale&facet=adresse_postale_comm" />
            </MapContainer>
        </div>
    )
}
