import React, { useEffect, useState } from "react"

export default function Apropos() {
    const [html, setHtml] = useState("")

    useEffect(() => {
        document.title = "À propos"
        fetch("/api/apropos")
            .then(r => r.json())
            .then(data => {
                if (data.html) setHtml(data.html)
            })
        window.scrollTo(0, 0)
    }, [])

    return <div className="content" dangerouslySetInnerHTML={{ __html: html }}></div>
}
