import React, { useEffect } from "react"
import { Link } from "react-router-dom"
import { MapContainer } from "react-leaflet"
import MapComponent from "../lib/MapComponent"
import { useMatomo } from "@m4tt72/matomo-tracker-react"

export default function Confinement() {
    const { trackPageView } = useMatomo()

    useEffect(() => {
        document.title = "Confinement"
        trackPageView()
    }, [trackPageView])

    return (
        <div className="container">
            <div className="level">
                <div className="level-left">
                    <div className="title">Associations actives pendant le confinement</div>
                </div>
                <div className="level-right">
                    <Link to="/aidealimentaire">Aide alimentaire</Link>
                </div>
            </div>
            <MapContainer
                center={[0, 0]}
                zoom={13}
                maxZoom={18}
                style={{ width: "100%", height: "600px", zIndex: "1" }}>
                <MapComponent dataUrl="/api/confinement/markers" />
            </MapContainer>
        </div>
    )
}
