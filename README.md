Annuaire des Assos
==================

L’annuaire des associations est un service mis en place au sein de la Maison des Associations, par l’association BUG, dont le siège social se situe à Rennes.

L’annuaire a pour vocation de répertorier les associations et les collectifs d’habitants mettant en place des projets d’interêt général à but non-lucratif sur le territoire rennais. Il permet de communiquer aux utilisateurs un ensemble d’informations sur les activités des associations répertoriées, qui leur permet par la suite de prendre contact avec la ou les associations concernées.

La création d’un compte et l’enregistrement d’une association ou d’un collectif sont basés sur l’initiative des administrateurs de l’association.

Le service propose une géolocalisation basé sur un fonds de carte Openstreetmap.

L’annuaire est un service rattaché au site mda-rennes.org de la Maison des Associations.

Ce service a pour objectif principal de permettre la recherche des activités associatives par des habitants en recherche de services (activités, bénévolat…).
