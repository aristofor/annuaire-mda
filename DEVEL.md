Annuaire des assos - Développement
==================================

### fichier `local/annuaire_settings.py`

~~~
BACKDOOR_SECRET = < python -c 'from werkzeug.security import generate_password_hash; print(generate_password_hash("secret"))' >
~~~


Lancement des serveurs
----------------------

### Appli annuaire
~~~
$ . local/environ
$ flask run
~~~

### Serveur admin
~~~
$ python3 -m annuaire.admin
~~~

### Vider les caches python
~~~
make clean
~~~

### Vidage du cache redis
~~~
$ redis-cli
127.0.0.1:6379> FLUSHDB
~~~

Thème et feuille CSS
--------------------

Reconstruit la feuille de styles
~~~
make theme
~~~
[doc bulma](https://bulma.io/documentation/)


Ressources
----------

[doc leaflet](https://leafletjs.com/reference-1.6.0.html)

[api géocodage](https://geo.api.gouv.fr/adresse)
