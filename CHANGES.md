CHANGES
=======

* flag Entry.active
* React Router v6
* postgresql text vector remplace Whoosh
* `yarn format` remplace `make prettier`
